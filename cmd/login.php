<?php


// entende o PATH RAIZ do site

$path_raiz = str_replace( ( ( strpos( $_SERVER[ 'SCRIPT_NAME' ], '/~' ) !== false ) ? substr( $_SERVER[ 'SCRIPT_NAME' ], strpos( $_SERVER[ 'SCRIPT_NAME' ], '/', 1 ) ) : $_SERVER[ 'SCRIPT_NAME' ] ), '', $_SERVER[ 'SCRIPT_FILENAME' ] );


// carrega bibliotecas, demais funções e variáveis

require( $path_raiz.'/conn/requires_cmd.php' );


// recebe parâmetros

$param = isset( $_POST[ $POST_params[ 'a0' ] ] ) ? $_POST[ $POST_params[ 'a0' ] ] : NULL;
if ( !$param ) { echo 'falha no parâmetro';	exit; }


// entende parâmetros

$p = entendeParam( $param );
$usuario = getParamAndNext( $p );
$senha = getParamAndNext( $p );


// preenche variáveis necessárias para login

$datahora = date( 'Y-m-d H:i:s' );

if ( getenv( 'HTTP_X_FORWARDED_FOR' ) ) {
	$pipaddress = getenv( 'HTTP_X_FORWARDED_FOR' );
	$ipaddress = getenv( 'REMOTE_ADDR' );
	$ip = $pipaddress." via proxi ".$ipaddress;
	$host = gethostbyaddr( $ipaddress );
}	else {
	$ipaddress = getenv( 'REMOTE_ADDR' );
	$ip = $ipaddress;
	$host = gethostbyaddr( $ipaddress );
}

$navegador = getenv( "HTTP_USER_AGENT" );
$cookie = 0;


// consulta usuário e faz login

try {
	inicia_transacao( $conexao, $transaction );
	
	
	// verifica o padrão do hash para este usuário. Caso seja o antigo, atualiza a senha e o padrão para o novo.
	
	$sql = 'SELECT USU_ID, USU_SALT FROM USUARIOS WHERE USU_USUARIO = :usuario;';
	$qry = $conexao->prepare( $sql );
	$qry->bindParam( ':usuario', $usuario);
	$qry->execute();
	$r = $qry->fetch( PDO::FETCH_ASSOC );
	$usu_id = $r[ 'USU_ID' ];
	$usu_salt = $r[ 'USU_SALT' ];

	
	// continua com processo de autenticação

	$senha_hash = criptografar_whirlpool__doubleSalt( $usu_salt, $senha );

	$sql = 'CALL P_LOGIN ( :usuario, :senha, :datahora, :ip, :host, :navegador, @usu_id, @usu_nome, @car_id, @car_nome );';
	$qry = $conexao->prepare( $sql );
	$qry->bindParam( ':usuario', $usuario );
	$qry->bindParam( ':senha', $senha_hash );
	$qry->bindParam( ':datahora', $datahora );
	$qry->bindParam( ':ip', $ip );
	$qry->bindParam( ':host', $host );
	$qry->bindParam( ':navegador', $navegador );
	$qry->execute();

	$qry = $conexao->prepare( "SELECT @usu_id AS USU_ID, @usu_nome AS USU_NOME;" );
	$qry->execute();
	$r = $qry->fetch( PDO::FETCH_ASSOC );
	$usu_id = $r[ 'USU_ID' ];
	$usu_nome = $r[ 'USU_NOME' ];


	commit_transacao( $conexao, $transaction );
} catch ( Exception $e ) { rollback_transacao( $conexao, $transaction, $e->getMessage() ); }


// se usuário foi logado

if ( $usu_id ) {


	// carrega variáveis de sessão

	session_start();

	$_SESSION[ 'LGN_UsuId' ] = $usu_id;
	$_SESSION[ 'LGN_UsuNome' ] = $usu_nome;

	$_SESSION[ 'HTTP_USER_AGENT' ] = md5( $_SERVER[ 'HTTP_USER_AGENT' ] );

	unset( $_SESSION[ 'LGN_ErrosLoginSeguidos' ] );

	session_write_close();
	
	
	// mantém a informação da sessão também em cookie

	criar_cookie( 'LGN_UsuId', $_SESSION[ 'LGN_UsuId' ] );			
	criar_cookie( 'LGN_UsuNome', $_SESSION[ 'LGN_UsuNome' ] );
	criar_cookie( 'HUA', $_SESSION[ 'HTTP_USER_AGENT' ] );


	// retorno

	echo 'ok'.$sepParam.$POST_init_param;
	exit;

}
else {

	echo 'se';

}






