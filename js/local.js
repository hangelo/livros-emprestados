

// habilita ou desabilita os botões

function disable_objs( val ) {
	obj_nome.disabled = val;
	obj_titulo.disabled = val;
	obj_chk_dom.disabled = val;
	obj_abre_dom.disabled = val;
	obj_fecha_dom.disabled = val;
	obj_chk_seg.disabled = val;
	obj_abre_seg.disabled = val;
	obj_fecha_seg.disabled = val;
	obj_chk_ter.disabled = val;
	obj_abre_ter.disabled = val;
	obj_fecha_ter.disabled = val;
	obj_chk_qua.disabled = val;
	obj_abre_qua.disabled = val;
	obj_fecha_qua.disabled = val;
	obj_chk_qui.disabled = val;
	obj_abre_qui.disabled = val;
	obj_fecha_qui.disabled = val;
	obj_chk_sex.disabled = val;
	obj_abre_sex.disabled = val;
	obj_fecha_sex.disabled = val;
	obj_chk_sab.disabled = val;
	obj_abre_sab.disabled = val;
	obj_fecha_sab.disabled = val;
	obj_estado.disabled = val;
	obj_cidade.disabled = val;
	obj_endereco.disabled = val;
	obj_latitude.disabled = val;
	obj_longitude.disabled = val;
	obj_origem_url.disabled = val;
	obj_origem_nome.disabled = val;
	obj_contato.disabled = val;
	obj_desc_resumida.disabled = val;
	obj_desc_completa.disabled = val;
	obj_inf_adicionais.disabled = val;
	obj_conteudo.disabled = val;
}


// pega os valores das caixas de texto e retorna em uma string formando protocolo de envio

function get_objsVal() {
	var p = '';
	p += sepParam + encodeParam( obj_nome.value );
	p += sepParam + encodeParam( obj_titulo.value );
	p += sepParam + encodeParam( obj_chk_dom.value );
	p += sepParam + encodeParam( obj_abre_dom.value );
	p += sepParam + encodeParam( obj_fecha_dom.value );
	p += sepParam + encodeParam( obj_chk_seg.value );
	p += sepParam + encodeParam( obj_abre_seg.value );
	p += sepParam + encodeParam( obj_fecha_seg.value );
	p += sepParam + encodeParam( obj_chk_ter.value );
	p += sepParam + encodeParam( obj_abre_ter.value );
	p += sepParam + encodeParam( obj_fecha_ter.value );
	p += sepParam + encodeParam( obj_chk_qua.value );
	p += sepParam + encodeParam( obj_abre_qua.value );
	p += sepParam + encodeParam( obj_fecha_qua.value );
	p += sepParam + encodeParam( obj_chk_qui.value );
	p += sepParam + encodeParam( obj_abre_qui.value );
	p += sepParam + encodeParam( obj_fecha_qui.value );
	p += sepParam + encodeParam( obj_chk_sex.value );
	p += sepParam + encodeParam( obj_abre_sex.value );
	p += sepParam + encodeParam( obj_fecha_sex.value );
	p += sepParam + encodeParam( obj_chk_sab.value );
	p += sepParam + encodeParam( obj_abre_sab.value );
	p += sepParam + encodeParam( obj_fecha_sab.value );
	p += sepParam + encodeParam( obj_estado.value );
	p += sepParam + encodeParam( obj_cidade.value );
	p += sepParam + encodeParam( obj_endereco.value );
	p += sepParam + encodeParam( obj_latitude.value );
	p += sepParam + encodeParam( obj_longitude.value );
	p += sepParam + encodeParam( obj_origem_url.value );
	p += sepParam + encodeParam( obj_origem_nome.value );
	p += sepParam + encodeParam( tinyMCE.get( 'contato' ).getContent() );
	p += sepParam + encodeParam( obj_desc_resumida.value );
	p += sepParam + encodeParam( obj_desc_completa.value );
	p += sepParam + encodeParam( tinyMCE.get( 'inf_adicionais' ).getContent() );
	p += sepParam + encodeParam( tinyMCE.get( 'conteudo' ).getContent() );
	return p;
}


// lnsere novo local

var ex_locAdd = getXmlDoc();

function locAdd() {
	
	setBtnWait( document.getElementById( 'btnSalvar' ), true );

	disable_objs( true );
	
	var p = get_objsVal();
	var params = POSTinitP + jsP_a0 + encodeParam( loc_tip_id ) + jsP_b0 + p;

	ex_locAdd.open( "POST", "/pg/telas/locais/locAdd.php", true );
	ex_locAdd.setRequestHeader( "Content-type", "application/x-www-form-urlencoded" );
	ex_locAdd.send( params );
}

ex_locAdd.addEventListener( "progress", _ex_locAdd_updateProgress );
ex_locAdd.addEventListener( "load", _ex_locAdd_transferComplete );
ex_locAdd.addEventListener( "error", _ex_locAdd_transferFailed );
ex_locAdd.addEventListener( "abort", _ex_locAdd_transferCanceled );

function _ex_locAdd_updateProgress( oEvent ) {
  if ( oEvent.lengthComputable ) {
    var percentComplete = oEvent.loaded / oEvent.total;
    console.log( 'progresso: ' + percentComplete );
  } else {
  	console.log( 'não pode computar progresso' );
  }
}

function _ex_locAdd_transferComplete( evt ) {
	if ( this.readyState != 4 || this.status != 200 ) {
		console.log( 'falha' );
		return;
	}

  var r = String( this.responseText );
  var x = String( r ).split( '|' );

  if ( x[ 0 ] == 'ok' ) window.location.href = '/?' + x[ 1 ];
  else alert( 'erro' );
}

function _ex_locAdd_transferFailed( evt ) {
  console.log( 'An error occurred while transferring the file.' );
}

function _ex_locAdd_transferCanceled( evt ) {
  console.log( 'The transfer has been canceled by the user.' );
}


// Edita um local

var ex_locEdit = getXmlDoc();

function locEdit() {
	
	setBtnWait( document.getElementById( 'btnSalvar' ), true );

	disable_objs( true );
	
	var p = get_objsVal();
	var params = POSTinitP + jsP_a0 + encodeParam( loc_tip_id ) + jsP_b0 + encodeParam( loc_id ) + jsP_c0 + p;

	ex_locEdit.open( "POST", "/pg/telas/locais/locEdit.php", true );
	ex_locEdit.setRequestHeader( "Content-type", "application/x-www-form-urlencoded" );
	ex_locEdit.send( params );
}

ex_locEdit.addEventListener( "progress", _ex_locEdit_updateProgress );
ex_locEdit.addEventListener( "load", _ex_locEdit_transferComplete );
ex_locEdit.addEventListener( "error", _ex_locEdit_transferFailed );
ex_locEdit.addEventListener( "abort", _ex_locEdit_transferCanceled );

function _ex_locEdit_updateProgress( oEvent ) {
  if ( oEvent.lengthComputable ) {
    var percentComplete = oEvent.loaded / oEvent.total;
    console.log( 'progresso: ' + percentComplete );
  } else {
  	console.log( 'não pode computar progresso' );
  }
}

function _ex_locEdit_transferComplete( evt ) {
	if ( this.readyState != 4 || this.status != 200 ) {
		console.log( 'falha' );
		return;
	}
	
  var r = String( this.responseText );
  var x = String( r ).split( '|' );

	if ( x[ 0 ] == 'ok' ) window.location.reload();
	else alert( 'erro' );
}

function _ex_locEdit_transferFailed( evt ) {
  console.log( 'An error occurred while transferring the file.' );
}

function _ex_locEdit_transferCanceled( evt ) {
  console.log( 'The transfer has been canceled by the user.' );
}


// faz upload de foto do lote

function upload_foto_loc() {
	var vFD = new FormData( document.getElementById( 'frm_loc_capa' ) );
	var oXHR = new XMLHttpRequest();       
	oXHR.addEventListener( 'load', uploadFinish_capa_catalogo, false );
	oXHR.upload.addEventListener( 'progress', uploadProgress_capa_catalogo, false );
	oXHR.open( 'POST', '/pg/telas/locais/loc_upload_foto.php' );
	oXHR.send( vFD );
}

function uploadFinish_capa_catalogo( e ) {
	//document.getElementById( 'img_loc_capa' ).src = e.target.responseText;
	//document.getElementById( 'btn_foto_upload' ).innerHTML = 'procurar arquivo';
	window.location.reload();
}

function uploadProgress_capa_catalogo( e ) {
	var p = parseInt( e.loaded / e.total * 100 );
	document.getElementById( 'btn_foto_upload' ).innerHTML = p + '% enviando...';
}


// remove uma imagem

var ex_delFot = ( window.XMLHttpRequest ) ? new XMLHttpRequest() : new ActiveXObject( "Microsoft.XMLHTTP" );			// adiciona um local de hospedagem

function delete_img( loc_fot_id ) {

	var params = POSTinitP + jsP_a0 + loc_fot_id;
	ex_delFot.open( "POST", "/pg/telas/locais/loc_fot_del.php", true );
	ex_delFot.setRequestHeader( "Content-type", "application/x-www-form-urlencoded" );
	ex_delFot.send( params );
}

ex_delFot.onreadystatechange = _ex_delFot;
function _ex_delFot() {
	if ( ex_delFot.readyState == 4 && ex_delFot.status == 200 ) {
		var resp = String( ex_delFot.responseText );
		if ( resp == 'ok' ) window.location.reload();
	}
}


// promove uma imagem à capa do local

var ex_priFot = ( window.XMLHttpRequest ) ? new XMLHttpRequest() : new ActiveXObject( "Microsoft.XMLHTTP" );			// adiciona um local de hospedagem

function destaque_img( loc_fot_id ) {
	var params = POSTinitP + jsP_a0 + loc_fot_id;
	
	ex_priFot.open( "POST", "/pg/telas/locais/loc_fot_pri.php", true );
	ex_priFot.setRequestHeader( "Content-type", "application/x-www-form-urlencoded" );
	ex_priFot.send( params );
}

ex_priFot.onreadystatechange = func__ex_priFot;
function func__ex_priFot() {
	if ( ex_priFot.readyState == 4 && ex_priFot.status == 200 ) {
		var resp = String( ex_priFot.responseText );
		if ( resp == 'ok' ) window.location.reload();
	}
}


// faz o download de todas as fotos

function download_fotos() {
	open( '/pg/telas/locais/download_fotos.php?' + POSTinitP + jsP_a0 + encodeParam( loc_id ), '_blank' );
}


