





function _setCookie( c_name, value, exdays ) {
	var exdate = new Date();
	exdate.setDate( exdate.getDate() + exdays );
	var c_value = escape( value ) + ( ( exdays == null ) ? "" : "; expires=" + exdate.toUTCString() ) + '; domain=' + _DOMAIN_url + '; path=/';
	document.cookie = c_name + "=" + c_value;
}

function setCookie( c_name, value ) {
	_setCookie( c_name, value, 1 );
}

function setCookie_mes( c_name, value ) {
	_setCookie( c_name, value, 30 );
}

function getCookie( c_name ) {
	var i, x, y, ARRcookies = document.cookie.split( ";" );
	for ( i = 0; i < ARRcookies.length; i++ )
	{
		x = ARRcookies[ i ].substr( 0, ARRcookies[ i ].indexOf( "=" ) );
		y = ARRcookies[ i ].substr( ARRcookies[ i ].indexOf( "=" ) + 1 );
		x = x.replace( /^\s+|\s+$/g, "" );
		if ( x == c_name ) {
			return unescape( y );
		}
	}
}



