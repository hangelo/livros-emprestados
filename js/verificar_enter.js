function verifica_enter(evt) {
	var key_code = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which ? evt.which : void 0;
	return ( key_code == 13 );
}
