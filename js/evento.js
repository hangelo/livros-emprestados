

// habilita ou desabilita os botões

function disable_objs( val ) {
	obj_nome.disabled = val;
	obj_titulo.disabled = val;
	obj_genero.disabled = val;
	obj_data_inicio.disabled = val;
	obj_data_termino.disabled = val;
	obj_nome_local.disabled = val;
	obj_chk_dom.disabled = val;
	obj_abre_dom.disabled = val;
	obj_fecha_dom.disabled = val;
	obj_chk_seg.disabled = val;
	obj_abre_seg.disabled = val;
	obj_fecha_seg.disabled = val;
	obj_chk_ter.disabled = val;
	obj_abre_ter.disabled = val;
	obj_fecha_ter.disabled = val;
	obj_chk_qua.disabled = val;
	obj_abre_qua.disabled = val;
	obj_fecha_qua.disabled = val;
	obj_chk_qui.disabled = val;
	obj_abre_qui.disabled = val;
	obj_fecha_qui.disabled = val;
	obj_chk_sex.disabled = val;
	obj_abre_sex.disabled = val;
	obj_fecha_sex.disabled = val;
	obj_chk_sab.disabled = val;
	obj_abre_sab.disabled = val;
	obj_fecha_sab.disabled = val;
	obj_estado.disabled = val;
	obj_cidade.disabled = val;
	obj_endereco.disabled = val;
	obj_origem_url.disabled = val;
	obj_origem_nome.disabled = val;
	obj_contato.disabled = val;
	obj_desc_resumida.disabled = val;
	obj_desc_completa.disabled = val;
	obj_inf_adicionais.disabled = val;
	obj_inf_pagamento.disabled = val;
	obj_conteudo.disabled = val;
}


// pega os valores das caixas de texto e retorna em uma string formando protocolo de envio

function get_objsVal() {
	var p = '';
	p += sepParam + encodeParam( obj_nome.value );
	p += sepParam + encodeParam( obj_titulo.value );
	p += sepParam + encodeParam( obj_genero.value );
	p += sepParam + encodeParam( obj_data_inicio.value );
	p += sepParam + encodeParam( obj_data_termino.value );
	p += sepParam + encodeParam( obj_nome_local.value );
	p += sepParam + encodeParam( obj_chk_dom.value );
	p += sepParam + encodeParam( obj_abre_dom.value );
	p += sepParam + encodeParam( obj_fecha_dom.value );
	p += sepParam + encodeParam( obj_chk_seg.value );
	p += sepParam + encodeParam( obj_abre_seg.value );
	p += sepParam + encodeParam( obj_fecha_seg.value );
	p += sepParam + encodeParam( obj_chk_ter.value );
	p += sepParam + encodeParam( obj_abre_ter.value );
	p += sepParam + encodeParam( obj_fecha_ter.value );
	p += sepParam + encodeParam( obj_chk_qua.value );
	p += sepParam + encodeParam( obj_abre_qua.value );
	p += sepParam + encodeParam( obj_fecha_qua.value );
	p += sepParam + encodeParam( obj_chk_qui.value );
	p += sepParam + encodeParam( obj_abre_qui.value );
	p += sepParam + encodeParam( obj_fecha_qui.value );
	p += sepParam + encodeParam( obj_chk_sex.value );
	p += sepParam + encodeParam( obj_abre_sex.value );
	p += sepParam + encodeParam( obj_fecha_sex.value );
	p += sepParam + encodeParam( obj_chk_sab.value );
	p += sepParam + encodeParam( obj_abre_sab.value );
	p += sepParam + encodeParam( obj_fecha_sab.value );
	p += sepParam + encodeParam( obj_estado.value );
	p += sepParam + encodeParam( obj_cidade.value );
	p += sepParam + encodeParam( obj_endereco.value );
	p += sepParam + encodeParam( obj_origem_url.value );
	p += sepParam + encodeParam( obj_origem_nome.value );
	p += sepParam + encodeParam( obj_contato.value );
	p += sepParam + encodeParam( obj_desc_resumida.value );
	p += sepParam + encodeParam( obj_desc_completa.value );
	p += sepParam + encodeParam( obj_inf_adicionais.value );
	p += sepParam + encodeParam( obj_inf_pagamento.value );
	p += sepParam + encodeParam( tinyMCE.get( 'conteudo' ).getContent() );
	return p;
}


// lnsere novo evento

var ex_eveAdd = getXmlDoc();

function eveAdd() {
	
	setBtnWait( document.getElementById( 'btnSalvar' ), true );

	disable_objs( true );
	
	var p = get_objsVal();
	var params = POSTinitP + jsP_a0 + encodeParam( eve_tip_id ) + jsP_b0 + p;

	ex_eveAdd.open( "POST", "/pg/telas/eventos/eveAdd.php", true );
	ex_eveAdd.setRequestHeader( "Content-type", "application/x-www-form-urlencoded" );
	ex_eveAdd.send( params );
}

ex_eveAdd.addEventListener( "progress", _ex_eveAdd_updateProgress );
ex_eveAdd.addEventListener( "load", _ex_eveAdd_transferComplete );
ex_eveAdd.addEventListener( "error", _ex_eveAdd_transferFailed );
ex_eveAdd.addEventListener( "abort", _ex_eveAdd_transferCanceled );

function _ex_eveAdd_updateProgress( oEvent ) {
  if ( oEvent.lengthComputable ) {
    var percentComplete = oEvent.loaded / oEvent.total;
    console.log( 'progresso: ' + percentComplete );
  } else {
  	console.log( 'não pode computar progresso' );
  }
}

function _ex_eveAdd_transferComplete( evt ) {
	if ( this.readyState != 4 || this.status != 200 ) {
		console.log( 'falha' );
		return;
	}

  var r = String( this.responseText );
  var x = String( r ).split( '|' );

  if ( x[ 0 ] == 'ok' ) window.location.href = '/?' + x[ 1 ];
  else alert( 'erro' );
}

function _ex_eveAdd_transferFailed( evt ) {
  console.log( 'An error occurred while transferring the file.' );
}

function _ex_eveAdd_transferCanceled( evt ) {
  console.log( 'The transfer has been canceled by the user.' );
}


// Edita um evento

var ex_eveEdit = getXmlDoc();

function eveEdit() {
	
	setBtnWait( document.getElementById( 'btnSalvar' ), true );

	disable_objs( true );
	
	var p = get_objsVal();
	var params = POSTinitP + jsP_a0 + encodeParam( eve_tip_id ) + jsP_b0 + encodeParam( eve_id ) + jsP_c0 + p;

	ex_eveEdit.open( "POST", "/pg/telas/eventos/eveEdit.php", true );
	ex_eveEdit.setRequestHeader( "Content-type", "application/x-www-form-urlencoded" );
	ex_eveEdit.send( params );
}

ex_eveEdit.addEventListener( "progress", _ex_eveEdit_updateProgress );
ex_eveEdit.addEventListener( "load", _ex_eveEdit_transferComplete );
ex_eveEdit.addEventListener( "error", _ex_eveEdit_transferFailed );
ex_eveEdit.addEventListener( "abort", _ex_eveEdit_transferCanceled );

function _ex_eveEdit_updateProgress( oEvent ) {
  if ( oEvent.lengthComputable ) {
    var percentComplete = oEvent.loaded / oEvent.total;
    console.log( 'progresso: ' + percentComplete );
  } else {
  	console.log( 'não pode computar progresso' );
  }
}

function _ex_eveEdit_transferComplete( evt ) {
	if ( this.readyState != 4 || this.status != 200 ) {
		console.log( 'falha' );
		return;
	}
	
  var r = String( this.responseText );
  var x = String( r ).split( '|' );

	if ( x[ 0 ] == 'ok' ) window.location.reload();
	else alert( 'erro' );
}

function _ex_eveEdit_transferFailed( evt ) {
  console.log( 'An error occurred while transferring the file.' );
}

function _ex_eveEdit_transferCanceled( evt ) {
  console.log( 'The transfer has been canceled by the user.' );
}


// faz upload de foto do lote

function upload_foto_eve() {
	var vFD = new FormData( document.getElementById( 'frm_eve_capa' ) );
	var oXHR = new XMLHttpRequest();       
	oXHR.addEventListener( 'load', uploadFinish_capa_catalogo, false );
	oXHR.upload.addEventListener( 'progress', uploadProgress_capa_catalogo, false );
	oXHR.open( 'POST', '/pg/telas/eventos/eve_upload_foto.php' );
	oXHR.send( vFD );
}

function uploadFinish_capa_catalogo( e ) {
	//document.getElementById( 'img_eve_capa' ).src = e.target.responseText;
	//document.getElementById( 'btn_foto_upload' ).innerHTML = 'procurar arquivo';
	window.location.reload();
}

function uploadProgress_capa_catalogo( e ) {
	var p = parseInt( e.loaded / e.total * 100 );
	document.getElementById( 'btn_foto_upload' ).innerHTML = p + '% enviando...';
}


// remove uma imagem

var ex_delFot = ( window.XMLHttpRequest ) ? new XMLHttpRequest() : new ActiveXObject( "Microsoft.XMLHTTP" );			// adiciona um evento de hospedagem

function delete_img( eve_fot_id ) {

	var params = POSTinitP + jsP_a0 + eve_fot_id;
	ex_delFot.open( "POST", "/pg/telas/eventos/eve_fot_del.php", true );
	ex_delFot.setRequestHeader( "Content-type", "application/x-www-form-urlencoded" );
	ex_delFot.send( params );
}

ex_delFot.onreadystatechange = _ex_delFot;
function _ex_delFot() {
	if ( ex_delFot.readyState == 4 && ex_delFot.status == 200 ) {
		var resp = String( ex_delFot.responseText );
		if ( resp == 'ok' ) window.location.reload();
	}
}


// promove uma imagem à capa do evento

var ex_priFot = ( window.XMLHttpRequest ) ? new XMLHttpRequest() : new ActiveXObject( "Microsoft.XMLHTTP" );			// adiciona um evento de hospedagem

function destaque_img( eve_fot_id ) {
	var params = POSTinitP + jsP_a0 + eve_fot_id;
	
	ex_priFot.open( "POST", "/pg/telas/eventos/eve_fot_pri.php", true );
	ex_priFot.setRequestHeader( "Content-type", "application/x-www-form-urlencoded" );
	ex_priFot.send( params );
}

ex_priFot.onreadystatechange = func__ex_priFot;
function func__ex_priFot() {
	if ( ex_priFot.readyState == 4 && ex_priFot.status == 200 ) {
		var resp = String( ex_priFot.responseText );
		if ( resp == 'ok' ) window.location.reload();
	}
}




