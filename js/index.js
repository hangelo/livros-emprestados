

// adiciona um livro ao banco de dados

var an = angular.module( 'livFicha', [] );
an.controller( 'livController', [
	'$scope',
	'$http',
	function( $scope, $http ) { 


		/* adição */

		this.postForm_adicionar = function() {

			var p = '';
			var _url = '';
			var params = '';

			if ( $scope.livAdd_livId ) {
				p += sepParam + encodeParam( $scope.livAdd_livId );
				p += sepParam + encodeParam( this.tit );
				params = POSTinitP + jsP_a0 + p;
				_url = '/pg/telas/livros/livEdit.php';
			}
			else {
				p += sepParam + encodeParam( this.tit );
				params = POSTinitP + jsP_a0 + p;
				_url = '/pg/telas/livros/livAdd.php';
			}

			$http( {
				method: 'POST',
				url: _url,
				data: params,
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			} ).success( function( data ) {

			  var r = String( data.trim() );
			  var x = String( r ).split( sepParam );

			  if ( x[ 0 ] == 'ok' ) {
			  	$scope.tit = '';
			  	window.location.reload();					
			  }
			  else {
			  	$scope.erMsg = 'Estamos com problemas. Entre em contato com o telefone 999';
			  }

			} );
		}

		this.cancelForm_adicionar = function() {
			$scope.livAdd_livId = '';
			$scope.livCtrl.tit = '';
			document.getElementById( 'livFicha' ).style.display = 'none';
			document.getElementById( 'livFicha_bg' ).style.display = 'none';			
		}


		/* edição */

		this.doEdit = function( liv_id, titulo ) {

			$scope.livAdd_livId = liv_id;
			$scope.livCtrl.tit = decodeURIComponent( titulo );
			document.getElementById( 'livFicha' ).style.display = 'block';
			document.getElementById( 'livFicha_bg' ).style.display = 'block';	

		}


		/* emprestimo */

		this.doEmprestar = function( liv_id, titulo ) {
			$scope.livTitulo_emprestar = 'Título: ' + decodeURIComponent( titulo );
			$scope.livEmp_livId = liv_id;
			document.getElementById( 'livEmprestar' ).style.display = 'block';
			document.getElementById( 'livEmprestar_bg' ).style.display = 'block';	
		}

		this.postForm_emprestar = function() {

			var p = '';
			p += sepParam + encodeParam( $scope.livEmp_livId );
			p += sepParam + encodeParam( this.quem );
			var params = POSTinitP + jsP_a0 + p;

			$http( {
				method: 'POST',
				url: '/pg/telas/livros/livEmp.php',
				data: params,
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			} ).success( function( data ) {

			  var r = String( data.trim() );
			  var x = String( r ).split( sepParam );

			  if ( x[ 0 ] == 'ok' ) {
			  	$scope.tit = '';
			  	window.location.reload();					
			  }
			  else {
			  	$scope.erMsg = 'Estamos com problemas. Entre em contato com o telefone 999';
			  }

			} );
		}

		this.cancelForm_emprestar = function() {

			document.getElementById( 'livEmprestar' ).style.display = 'none';
			document.getElementById( 'livEmprestar_bg' ).style.display = 'none';			
		}


		/* devolução */

		this.doDevolver = function( liv_id, titulo ) {
			$scope.livTitulo_devolver = 'Título: ' + titulo;
			$scope.livDev_livId = liv_id;
			document.getElementById( 'livDevolver' ).style.display = 'block';
			document.getElementById( 'livDevolver_bg' ).style.display = 'block';	
		}

		this.postForm_devolver = function() {

			var p = '';
			p += sepParam + encodeParam( $scope.livDev_livId );
			var params = POSTinitP + jsP_a0 + p;

			$http( {
				method: 'POST',
				url: '/pg/telas/livros/livDev.php',
				data: params,
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			} ).success( function( data ) {

			  var r = String( data.trim() );
			  var x = String( r ).split( sepParam );

			  if ( x[ 0 ] == 'ok' ) {
			  	$scope.tit = '';
			  	window.location.reload();					
			  }
			  else {
			  	$scope.erMsg = 'Estamos com problemas. Entre em contato com o telefone 999';
			  }

			} );
		}

		this.cancelForm_devolver = function() {

			document.getElementById( 'livDevolver' ).style.display = 'none';
			document.getElementById( 'livDevolver_bg' ).style.display = 'none';			
		}

	}
] );


// abre janela para novo livro

function add() {
	document.getElementById( 'livFicha' ).style.display = 'block';
	document.getElementById( 'livFicha_bg' ).style.display = 'block';
}


// emprestar

var ex_livEmp = getXmlDoc();
ex_livEmp.addEventListener( "load", _ex_livEmp_load );

function emprestar( liv_id ) {

	var params = POSTinitP + jsP_a0 + encodeParam( liv_id );

	ex_livEmp.open( "POST", "/pg/telas/livros/livEmp.php", true );
	ex_livEmp.setRequestHeader( "Content-type", "application/x-www-form-urlencoded" );
	ex_livEmp.send( params );
}

function _ex_livEmp_load( evt ) {
	if ( this.readyState != 4 || this.status != 200 ) {
		console.log( 'falha' );
		return;
	}

  var r = String( this.responseText );
  var x = String( r ).split( sepParam );

  if ( x[ 0 ] == 'ok' ) window.location.reload();
  else alert( 'erro' );
}


// devolver

var ex_livDev = getXmlDoc();
ex_livDev.addEventListener( "load", _ex_livDev_load );

function emprestar( liv_id ) {

	var params = POSTinitP + jsP_a0 + encodeParam( liv_id );

	ex_livDev.open( "POST", "/pg/telas/livros/livDev.php", true );
	ex_livDev.setRequestHeader( "Content-type", "application/x-www-form-urlencoded" );
	ex_livDev.send( params );
}

function _ex_livDev_load( evt ) {
	if ( this.readyState != 4 || this.status != 200 ) {
		console.log( 'falha' );
		return;
	}

  var r = String( this.responseText );
  var x = String( r ).split( sepParam );

  if ( x[ 0 ] == 'ok' ) window.location.reload();
  else alert( 'erro' );
}

