

// habilita ou desabilita os botões

function disable_objs( val ) {
	obj_nome.disabled = val;
	obj_titulo.disabled = val;
	obj_fonte.disabled = val;
	obj_desc_resumida.disabled = val;
	obj_conteudo.disabled = val;
}


// pega os valores das caixas de texto e retorna em uma string formando protocolo de envio

function get_objsVal() {
	var p = '';
	p += sepParam + encodeParam( obj_nome.value );
	p += sepParam + encodeParam( obj_titulo.value );
	p += sepParam + encodeParam( obj_fonte.value );
	p += sepParam + encodeParam( obj_desc_resumida.value );
	p += sepParam + encodeParam( tinyMCE.get( 'conteudo' ).getContent() );
	return p;
}


// lnsere novo noticia

var ex_notAdd = getXmlDoc();

function notAdd() {
	
	setBtnWait( document.getElementById( 'btnSalvar' ), true );

	disable_objs( true );
	
	var p = get_objsVal();
	var params = POSTinitP + jsP_b0 + p;

	ex_notAdd.open( "POST", "/pg/telas/noticias/notAdd.php", true );
	ex_notAdd.setRequestHeader( "Content-type", "application/x-www-form-urlencoded" );
	ex_notAdd.send( params );
}

ex_notAdd.addEventListener( "progress", _ex_notAdd_updateProgress );
ex_notAdd.addEventListener( "load", _ex_notAdd_transferComplete );
ex_notAdd.addEventListener( "error", _ex_notAdd_transferFailed );
ex_notAdd.addEventListener( "abort", _ex_notAdd_transferCanceled );

function _ex_notAdd_updateProgress( oEvent ) {
  if ( oEvent.lengthComputable ) {
    var percentComplete = oEvent.loaded / oEvent.total;
    console.log( 'progresso: ' + percentComplete );
  } else {
  	console.log( 'não pode computar progresso' );
  }
}

function _ex_notAdd_transferComplete( evt ) {
	if ( this.readyState != 4 || this.status != 200 ) {
		console.log( 'falha' );
		return;
	}

  var r = String( this.responseText );
  var x = String( r ).split( '|' );

  if ( x[ 0 ] == 'ok' ) window.location.href = '/?' + x[ 1 ];
  else alert( 'erro' );
}

function _ex_notAdd_transferFailed( evt ) {
  console.log( 'An error occurred while transferring the file.' );
}

function _ex_notAdd_transferCanceled( evt ) {
  console.log( 'The transfer has been canceled by the user.' );
}


// Edita um noticia

var ex_notEdit = getXmlDoc();

function notEdit() {
	
	setBtnWait( document.getElementById( 'btnSalvar' ), true );

	disable_objs( true );
	
	var p = get_objsVal();
	var params = POSTinitP + jsP_b0 + encodeParam( not_id ) + jsP_c0 + p;

	ex_notEdit.open( "POST", "/pg/telas/noticias/notEdit.php", true );
	ex_notEdit.setRequestHeader( "Content-type", "application/x-www-form-urlencoded" );
	ex_notEdit.send( params );
}

ex_notEdit.addEventListener( "progress", _ex_notEdit_updateProgress );
ex_notEdit.addEventListener( "load", _ex_notEdit_transferComplete );
ex_notEdit.addEventListener( "error", _ex_notEdit_transferFailed );
ex_notEdit.addEventListener( "abort", _ex_notEdit_transferCanceled );

function _ex_notEdit_updateProgress( oEvent ) {
  if ( oEvent.lengthComputable ) {
    var percentComplete = oEvent.loaded / oEvent.total;
    console.log( 'progresso: ' + percentComplete );
  } else {
  	console.log( 'não pode computar progresso' );
  }
}

function _ex_notEdit_transferComplete( evt ) {
	if ( this.readyState != 4 || this.status != 200 ) {
		console.log( 'falha' );
		return;
	}
	
  var r = String( this.responseText );
  var x = String( r ).split( '|' );

	if ( x[ 0 ] == 'ok' ) window.location.reload();
	else alert( 'erro' );
}

function _ex_notEdit_transferFailed( evt ) {
  console.log( 'An error occurred while transferring the file.' );
}

function _ex_notEdit_transferCanceled( evt ) {
  console.log( 'The transfer has been canceled by the user.' );
}


// faz upload de foto do lote

function upload_foto_not() {
	var vFD = new FormData( document.getElementById( 'frm_not_capa' ) );
	var oXHR = new XMLHttpRequest();       
	oXHR.addEventListener( 'load', uploadFinish_capa_catalogo, false );
	oXHR.upload.addEventListener( 'progress', uploadProgress_capa_catalogo, false );
	oXHR.open( 'POST', '/pg/telas/noticias/not_upload_foto.php' );
	oXHR.send( vFD );
}

function uploadFinish_capa_catalogo( e ) {
	//document.getElementById( 'img_not_capa' ).src = e.target.responseText;
	//document.getElementById( 'btn_foto_upload' ).innerHTML = 'procurar arquivo';
	window.location.reload();
}

function uploadProgress_capa_catalogo( e ) {
	var p = parseInt( e.loaded / e.total * 100 );
	document.getElementById( 'btn_foto_upload' ).innerHTML = p + '% enviando...';
}


// remove uma imagem

var ex_delFot = ( window.XMLHttpRequest ) ? new XMLHttpRequest() : new ActiveXObject( "Microsoft.XMLHTTP" );			// adiciona um noticia de hospedagem

function delete_img( not_fot_id ) {

	var params = POSTinitP + jsP_a0 + not_fot_id;
	ex_delFot.open( "POST", "/pg/telas/noticias/not_fot_del.php", true );
	ex_delFot.setRequestHeader( "Content-type", "application/x-www-form-urlencoded" );
	ex_delFot.send( params );
}

ex_delFot.onreadystatechange = _ex_delFot;
function _ex_delFot() {
	if ( ex_delFot.readyState == 4 && ex_delFot.status == 200 ) {
		var resp = String( ex_delFot.responseText );
		if ( resp == 'ok' ) window.location.reload();
	}
}


// promove uma imagem à capa do noticia

var ex_priFot = ( window.XMLHttpRequest ) ? new XMLHttpRequest() : new ActiveXObject( "Microsoft.XMLHTTP" );			// adiciona um noticia de hospedagem

function destaque_img( not_fot_id ) {
	var params = POSTinitP + jsP_a0 + not_fot_id;
	
	ex_priFot.open( "POST", "/pg/telas/noticias/not_fot_pri.php", true );
	ex_priFot.setRequestHeader( "Content-type", "application/x-www-form-urlencoded" );
	ex_priFot.send( params );
}

ex_priFot.onreadystatechange = func__ex_priFot;
function func__ex_priFot() {
	if ( ex_priFot.readyState == 4 && ex_priFot.status == 200 ) {
		var resp = String( ex_priFot.responseText );
		if ( resp == 'ok' ) window.location.reload();
	}
}




