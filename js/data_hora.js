function str_dataAtual() {
	var data = new Date();
	var dia = data.getDate();
	var mes = data.getMonth() + 1;
	var ano = data.getFullYear();
	var hora = data.getHours();
	var minuto = data.getMinutes();
	var data_atual = dia + '/' + ( ( String( mes ).length == 1 ) ? '0' : '' ) + mes + '/' + ( ( String( ano ).length == 1 ) ? '0' : '' ) + ano;
	var hora_atual = hora + ':' + minuto;
	
	return data_atual;
}

function str_horaAtual() {
	var data = new Date();
	var dia = data.getDate();
	var mes = data.getMonth() + 1;
	var ano = data.getFullYear();
	var hora = data.getHours();
	var minuto = data.getMinutes();
	var data_atual = dia + '/' + ( ( String( mes ).length == 1 ) ? '0' : '' ) + mes + '/' + ( ( String( ano ).length == 1 ) ? '0' : '' ) + ano;
	var hora_atual = hora + ':' + minuto;
	
	return hora_atual;
}

function SomarData( txtData, DiasAdd ) {
	
	txtData = String( txtData );
	DiasAdd = parseInt( DiasAdd );
	
	var d = new Date();
	d.setTime(Date.parse(txtData.split("/").reverse().join("/"))+(86400000*(DiasAdd)));
	var DataFinal;
	if ( d.getDate() < 10) 	{
		DataFinal = "0"+d.getDate().toString();
	}
	else
	{
		DataFinal = d.getDate().toString();    
	}

	if((d.getMonth()+1) < 10) {
		DataFinal += "/0"+(d.getMonth()+1).toString()+"/"+d.getFullYear().toString();
	}
	else {
		DataFinal += "/"+((d.getMonth()+1).toString())+"/"+d.getFullYear().toString();
	}
	return DataFinal;
}