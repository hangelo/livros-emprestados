

var an = angular.module( 'postLogin', [] );
an.controller( 'PostController', [
	'$scope', '$http', function( $scope, $http ) {        
		this.postForm = function() {

			var p = '';
			p += sepParam + encodeParam( this.inputData.us );
			p += sepParam + encodeParam( this.inputData.ps );
			var params = POSTinitP + jsP_a0 + p;

			$http( {
				method: 'POST',
				url: '/cmd/login.php',
				data: params,
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			} ).success( function( data ) {

			  var r = String( data.trim() );
			  var x = String( r ).split( sepParam );

			  if ( x[ 0 ] != 'ok' ) {
					if ( x[ 0 ] == 'se' )
						$scope.erMsg = "Usuário ou senha inválidos";
					else
						$scope.erMsg = "Infelismente estamos com problemas de acesso, por favor entre em contato através do telefone 999.";
			  }
			  else window.location.href = '/?' + x[ 1 ];

			} );
		}
	}
] );


