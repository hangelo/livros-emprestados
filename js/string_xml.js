
function XMLToString( oXML ) {
	if ( window.ActiveXObject ) { //code for IE
		var oString = oXML.xml;
		return oString;
	}
	
	else { // code for Chrome, Safari, Firefox, Opera, etc.
		return ( new XMLSerializer() ).serializeToString( oXML );
	}
}

function StringToXML( oString ) {
	if ( window.ActiveXObject ) { //code for IE
		var oXML = new ActiveXObject( "Microsoft.XMLDOM" );
		oXML.loadXML( oString );
		return oXML;
	}
	else { // code for Chrome, Safari, Firefox, Opera, etc.
		return ( new DOMParser() ).parseFromString( oString, "text/xml" );
	}
}
