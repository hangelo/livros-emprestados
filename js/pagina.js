

// habilita ou desabilita os botões

function disable_objs( val ) {
	obj_conteudo.disabled = val;
}


// pega os valores das caixas de texto e retorna em uma string formando protocolo de envio

function get_objsVal() {
	var p = '';
	p += sepParam + encodeParam( tinyMCE.get( 'conteudo' ).getContent() );
	return p;
}


// Edita um noticia

var ex_notEdit = getXmlDoc();

function notEdit() {
	
	setBtnWait( document.getElementById( 'btnSalvar' ), true );

	disable_objs( true );
	
	var p = get_objsVal();
	var params = POSTinitP + jsP_b0 + encodeParam( pag_id ) + jsP_c0 + p;

	ex_notEdit.open( "POST", "/pg/telas/paginas/pagEdit.php", true );
	ex_notEdit.setRequestHeader( "Content-type", "application/x-www-form-urlencoded" );
	ex_notEdit.send( params );
}

ex_notEdit.addEventListener( "progress", _ex_notEdit_updateProgress );
ex_notEdit.addEventListener( "load", _ex_notEdit_transferComplete );
ex_notEdit.addEventListener( "error", _ex_notEdit_transferFailed );
ex_notEdit.addEventListener( "abort", _ex_notEdit_transferCanceled );

function _ex_notEdit_updateProgress( oEvent ) {
  if ( oEvent.lengthComputable ) {
    var percentComplete = oEvent.loaded / oEvent.total;
    console.log( 'progresso: ' + percentComplete );
  } else {
  	console.log( 'não pode computar progresso' );
  }
}

function _ex_notEdit_transferComplete( evt ) {
	if ( this.readyState != 4 || this.status != 200 ) {
		console.log( 'falha' );
		return;
	}
	
  var r = String( this.responseText );
  var x = String( r ).split( '|' );

	if ( x[ 0 ] == 'ok' ) window.location.reload();
	else alert( 'erro' );
}

function _ex_notEdit_transferFailed( evt ) {
  console.log( 'An error occurred while transferring the file.' );
}

function _ex_notEdit_transferCanceled( evt ) {
  console.log( 'The transfer has been canceled by the user.' );
}




