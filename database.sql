

DROP TABLE IF EXISTS LIVROS;
CREATE TABLE LIVROS ( 
  LIV_ID INT NOT NULL AUTO_INCREMENT,
  LIV_DATAHORA_INSERIDO timestamp NULL,
  LIV_STATUS SMALLINT COMMENT '1=livro em mãos, não emprestado / 2=livro emprestado',
  LIV_TITULO VARCHAR( 255 ),
  LIV_NUMERO INT,
  LIV_ULT_EMPRESTIMO_SAIU DATE,
  LIV_ULT_EMPRESTIMO_VOLTOU DATE,
  PRIMARY KEY ( LIV_ID ),
  KEY I_LIV_TITULO ( LIV_TITULO ),
  KEY I_LIV_STATUS ( LIV_STATUS )
 ) ENGINE = InnoDB AUTO_INCREMENT = 0 DEFAULT CHARSET = utf8;

DELIMITER ;;
CREATE TRIGGER TR_LIV_BI BEFORE INSERT ON LIVROS FOR EACH ROW BEGIN
  IF ( NEW.LIV_DATAHORA_INSERIDO IS NULL ) THEN SET NEW.LIV_DATAHORA_INSERIDO = CURRENT_TIMESTAMP; END IF;
  IF ( NEW.LIV_STATUS IS NULL ) THEN SET NEW.LIV_STATUS = 1; END IF;
END
;;
DELIMITER ;



CREATE TABLE LIVROS_EMPRESTIMOS ( 
  LIV_EMP_ID INT NOT NULL AUTO_INCREMENT,
  LIV_ID INT NOT NULL,
  LIV_EMP_QUEM VARCHAR( 255 ),
  LIV_EMP_DATAHORA_SAIU TIMESTAMP,
  LIV_EMP_DATAHORA_VOLTOU TIMESTAMP,
  LIV_EMP_STATUS SMALLINT COMMENT '1=ainda emprestado / 2=já devolvido',
  PRIMARY KEY ( LIV_EMP_ID ),
  KEY I_LIV_EMP_STATUS ( LIV_EMP_STATUS ),
  KEY I_LIV_ID ( LIV_ID ),
  CONSTRAINT FK_LIV_ID FOREIGN KEY ( LIV_ID ) REFERENCES LIVROS ( LIV_ID ) ON DELETE CASCADE
 ) ENGINE = InnoDB AUTO_INCREMENT = 0 DEFAULT CHARSET = utf8;

DELIMITER ;;

DROP TRIGGER IF EXISTS TR_LIV_EMP_AU;
CREATE TRIGGER TR_LIV_EMP_AU AFTER UPDATE ON LIVROS_EMPRESTIMOS FOR EACH ROW BEGIN
  IF ( OLD.LIV_EMP_STATUS = 1 AND NEW.LIV_EMP_STATUS = 2 ) THEN
    UPDATE LIVROS SET LIV_STATUS = 1, LIV_ULT_EMPRESTIMO_VOLTOU = NEW.LIV_EMP_DATAHORA_VOLTOU WHERE LIV_ID = NEW.LIV_ID;
  END IF;
END
;;

DROP TRIGGER IF EXISTS TR_LIV_EMP_AI;
CREATE TRIGGER TR_LIV_EMP_AI AFTER INSERT ON LIVROS_EMPRESTIMOS FOR EACH ROW BEGIN
  IF ( NEW.LIV_EMP_STATUS = 1 ) THEN
    UPDATE LIVROS SET LIV_STATUS = 2, LIV_ULT_EMPRESTIMO_SAIU = NEW.LIV_EMP_DATAHORA_SAIU WHERE LIV_ID = NEW.LIV_ID;
  END IF;
END
;;

DELIMITER ;


/* tabela de usuários do sistema */

DROP TABLE IF EXISTS USUARIOS;
CREATE TABLE USUARIOS (
  USU_ID int NOT NULL AUTO_INCREMENT,
  USU_NOME varchar( 100 ),
  USU_USUARIO varchar( 100 ),
  USU_SENHA varchar( 128 ),
  USU_DATAHORA_CRIADO timestamp NULL,
  USU_DATAHORA_ULT_LOGIN timestamp NULL,
  USU_ERROS_SENHA int,
  USU_DATAHORA_ULT_TROCA_SENHA timestamp NULL,
  USU_PADRAO_HASH varchar( 20 ),
  USU_SALT varchar( 128 ),
  USU_TIMEZONE int,
  PRIMARY KEY ( USU_ID ),
  UNIQUE KEY AK_USU_USUARIO ( USU_USUARIO ),
  KEY I_USU_SENHA ( USU_USUARIO, USU_SENHA ),
) ENGINE = InnoDB AUTO_INCREMENT = 0 DEFAULT CHARSET = utf8;


CREATE PROCEDURE P_USU_ADD (
  p_nome varchar( 100 ),
  p_usuario varchar( 100 ),
  p_senha varchar( 128 ),
  p_salt varchar( 128 ),
  p_padrao varchar( 20 )
)
begin
  INSERT INTO USUARIOS ( USU_NOME, USU_USUARIO, USU_SENHA, USU_DATAHORA_CRIADO, USU_ERROS_SENHA, USU_PADRAO_HASH, USU_SALT )
  VALUES ( p_nome, p_usuario, p_senha, CURRENT_TIMESTAMP, 0, p_padrao, p_salt );
end;

CREATE PROCEDURE P_LOGIN (
  p_usuario varchar( 100 ),
  p_senha varchar( 128 ),
  p_datahora timestamp,
  p_ip varchar( 100 ),
  p_host varchar( 100 ),
  p_navegador varchar( 255 ),
  OUT p_usu_id bigint,
  OUT p_usu_nome varchar( 100 ),
  OUT p_car_id int,
  OUT p_car_nome varchar( 100 )
)
begin
    IF EXISTS( SELECT * FROM USUARIOS WHERE USU_USUARIO = p_usuario AND USU_SENHA = p_senha ) THEN
    
        SELECT USU_ID, USU_NOME FROM USUARIOS WHERE USU_USUARIO = p_usuario AND USU_SENHA = p_senha
        INTO p_usu_id, p_usu_nome;
        
        SET p_car_nome = ( SELECT CAR_NOME FROM CARGOS WHERE CAR_ID = p_car_id );
        
        UPDATE USUARIOS
        SET USU_DATAHORA_ULT_LOGIN = CURRENT_TIMESTAMP
        WHERE USU_ID = p_usu_id;
        
        INSERT INTO HIST_ACESSOS ( USU_ID, HIST_ACE_DATAHORA_LOGIN, HIST_ACE_IP, HIST_ACE_HOST, HIST_ACE_NAVEGADOR )
        VALUES ( p_usu_id, p_datahora, p_ip, p_host, p_navegador );
    ELSE
        SET p_usu_id = null;
        SET p_usu_nome = null;
        SET p_car_id = null;
        SET p_car_nome = null;
    END IF;
end;


/* mantém histórico de acessos */

DROP TABLE IF EXISTS HIST_ACESSOS;
CREATE TABLE HIST_ACESSOS ( 
  HIST_ACE_ID int NOT NULL AUTO_INCREMENT,
  USU_ID int NOT NULL,
  HIST_ACE_DATAHORA_LOGIN timestamp NULL,
  HIST_ACE_DATAHORA_LOGOUT timestamp NULL,
  HIST_ACE_IP varchar( 50 ),
  HIST_ACE_HOST varchar( 255 ),
  HIST_ACE_NAVEGADOR varchar( 255 ),
  PRIMARY KEY ( HIST_ACE_ID ),
  KEY I_USU_ID ( USU_ID ),
  CONSTRAINT FK_USU_ID FOREIGN KEY ( USU_ID ) REFERENCES USUARIOS ( USU_ID ) ON DELETE CASCADE
 ) ENGINE = InnoDB AUTO_INCREMENT = 0 DEFAULT CHARSET = utf8;



/* tabela genérica para histórico de eventos auditado */

DROP TABLE IF EXISTS HIST_PROCESSOS;
CREATE TABLE HIST_PROCESSOS ( 
  HIST_PROC_ID int NOT NULL AUTO_INCREMENT,
  USU_ID int NOT NULL,
  HIST_ACE_ID int NOT NULL,
  HIST_PROC_DATAHORA_EVENTO timestamp NULL,
  HIST_PROC_EVENTO varchar( 255 ),
  HIST_PROC_DESCRICAO text,
  PRIMARY KEY ( HIST_PROC_ID ),
  KEY I_USU_ID ( USU_ID ),
  KEY I_HIST_ACE_ID ( HIST_ACE_ID ),
  CONSTRAINT FK_USU_ID FOREIGN KEY ( USU_ID ) REFERENCES USUARIOS ( USU_ID ) ON DELETE CASCADE,
  CONSTRAINT FK_HIST_ACE_ID FOREIGN KEY ( HIST_ACE_ID ) REFERENCES HIST_ACESSOS ( HIST_ACE_ID ) ON DELETE CASCADE
 ) ENGINE = InnoDB AUTO_INCREMENT = 0 DEFAULT CHARSET = utf8;


/* mantém controle sobre o ID do livro em histórico de eventos auditados */

DROP TABLE IF EXISTS HIST_PROC_LIV;
CREATE TABLE HIST_PROC_LIV ( 
  HIST_PROC_ID int NOT NULL,
  LIV_ID int NOT NULL,
  PRIMARY KEY ( HIST_PROC_ID, LIV_ID ),
  CONSTRAINT FK_HIST_PROC_ID FOREIGN KEY ( HIST_PROC_ID ) REFERENCES HIST_PROCESSOS ( HIST_PROC_ID ) ON DELETE CASCADE,
  CONSTRAINT FK_LIV_ID FOREIGN KEY ( LIV_ID ) REFERENCES LIVROS ( LIV_ID ) ON DELETE CASCADE
 ) ENGINE = InnoDB AUTO_INCREMENT = 0 DEFAULT CHARSET = utf8;


/* modelo genérico para armazenar informação auditada */

CREATE PROCEDURE P_HIST_PROC_ADD ( 
  p_hist_ace_id int,
  p_usu_id int,
  p_evento varchar( 255 ),
  p_descricao text,
  OUT r_hist_proc_id int
)
BEGIN
  INSERT INTO HIST_PROCESSOS ( HIST_ACE_ID, USU_ID, HIST_PROC_DATAHORA_EVENTO, HIST_PROC_EVENTO, HIST_PROC_DESCRICAO )
  VALUES ( p_hist_ace_id, p_usu_id, CURRENT_TIMESTAMP, p_evento, p_descricao );
  SET r_hist_proc_id = ( SELECT MAX( HIST_PROC_ID ) FROM HIST_PROCESSOS );
END;


/* armazena informaçção em histórico de processos, vinculando-o ao ID do livro */

DROP PROCEDURE P_HIST_PROC_LIV;
CREATE PROCEDURE P_HIST_PROC_LIV ( 
  p_hist_ace_id int,
  p_usu_id int,
  p_evento varchar( 255 ),
  p_descricao text,
  p_liv_id_r int
)
begin
  declare v_hist_proc_id int;
  CALL P_HIST_PROC_ADD( p_hist_ace_id, p_usu_id, p_evento, p_descricao, v_hist_proc_id );
  INSERT INTO HIST_PROC_LIV( HIST_PROC_ID, LIV_ID ) VALUE ( v_hist_proc_id, p_liv_id_r );
END;


/* stored procedure para inserir um livro ao banco de dados */

DELIMITER ;;
CREATE PROCEDURE P_LIV_ADD( 
  p_usu_id int,
  p_titulo varchar( 255 ),
  OUT p_liv_id int
)
BEGIN


  /* variáveis para auditoria */

  declare v_hist_ace_id int;
  declare v_evento varchar( 255 );
  declare v_descricao text;


  /* insere livro */

  INSERT INTO LIVROS ( LIV_TITULO )
  VALUE ( p_titulo );

  SET p_liv_id = ( SELECT MAX( LIV_ID ) FROM LIVROS );


  /* salva histórico auditado */

  SET v_hist_ace_id = ( SELECT HIST_ACE_ID FROM HIST_ACESSOS WHERE USU_ID = p_usu_id ORDER BY HIST_ACE_ID DESC LIMIT 0, 1 );
  SET v_evento = CONCAT( 'Livro adicionado: "', p_titulo, '"' );
  SET v_descricao = CONCAT( 'Título do livro: "', p_titulo, '"', '|' );
  CALL P_HIST_PROC_LIV ( v_hist_ace_id, p_usu_id, v_evento, v_descricao, p_liv_id );

end
;;
DELIMITER ;


/* stored procedure para editar um livro no banco de dados */

DELIMITER ;;
CREATE PROCEDURE P_LIV_EDIT( 
  p_usu_id int,
  p_liv_id int,
  p_titulo varchar( 255 )
)
BEGIN


  /* variáveis para auditoria */

  declare v_hist_ace_id int;
  declare v_evento varchar( 255 );
  declare v_descricao text;


  /* insere livro */

  UPDATE LIVROS SET LIV_TITULO = p_titulo WHERE LIV_ID = p_liv_id;


  /* salva histórico auditado */

  SET v_hist_ace_id = ( SELECT HIST_ACE_ID FROM HIST_ACESSOS WHERE USU_ID = p_usu_id ORDER BY HIST_ACE_ID DESC LIMIT 0, 1 );
  SET v_evento = CONCAT( 'Livro editado: "', p_titulo, '"' );
  SET v_descricao = CONCAT( 'Novo título do livro: "', p_titulo, '"', '|' );
  CALL P_HIST_PROC_LIV ( v_hist_ace_id, p_usu_id, v_evento, v_descricao, p_liv_id );

end
;;
DELIMITER ;


/* stored procedure para emprestar um livro */

DELIMITER ;;
CREATE PROCEDURE P_LIV_EMP( 
  p_usu_id int,
  p_liv_id int,
  p_quem varchar( 255 ),
  p_datahora timestamp
)
BEGIN


  /* variáveis auxiliares */

  declare v_titulo varchar( 255 );


  /* variáveis para auditoria */

  declare v_hist_ace_id int;
  declare v_evento varchar( 255 );
  declare v_descricao text;


  /* pega título do livro */

  SET v_titulo = ( SELECT LIV_TITULO FROM LIVROS WHERE LIV_ID = p_liv_id );


  /* insere livro */

  INSERT INTO LIVROS_EMPRESTIMOS ( LIV_ID, LIV_EMP_QUEM, LIV_EMP_DATAHORA_SAIU, LIV_EMP_STATUS )
  VALUE ( p_liv_id, p_quem, p_datahora, 1 );


  /* salva histórico auditado */

  SET v_hist_ace_id = ( SELECT HIST_ACE_ID FROM HIST_ACESSOS WHERE USU_ID = p_usu_id ORDER BY HIST_ACE_ID DESC LIMIT 0, 1 );
  SET v_evento = CONCAT( 'Livro emprestado: "', v_titulo, '"' );
  SET v_descricao = CONCAT( 'Título do livro: "', v_titulo, '"', '|' );
  SET v_descricao = CONCAT( v_descricao, 'Para quem: "', p_quem, '"', '|' );
  SET v_descricao = CONCAT( v_descricao, 'Quando: "', p_datahora, '"', '|' );
  CALL P_HIST_PROC_LIV ( v_hist_ace_id, p_usu_id, v_evento, v_descricao, p_liv_id );

end
;;
DELIMITER ;





/* stored procedure para devolver um livro emprestado */

DELIMITER ;;
CREATE PROCEDURE P_LIV_DEV( 
  p_usu_id int,
  p_liv_id int,
  p_datahora timestamp
)
BEGIN


  /* variáveis auxiliares */

  declare v_titulo varchar( 255 );


  /* variáveis para auditoria */

  declare v_hist_ace_id int;
  declare v_evento varchar( 255 );
  declare v_descricao text;


  /* pega título do livro */

  SET v_titulo = ( SELECT LIV_TITULO FROM LIVROS WHERE LIV_ID = p_liv_id );


  /* insere livro */

  UPDATE LIVROS_EMPRESTIMOS SET LIV_EMP_DATAHORA_VOLTOU = p_datahora, LIV_EMP_STATUS = 2 WHERE LIV_ID = p_liv_id AND LIV_EMP_STATUS = 1;


  /* salva histórico auditado */

  SET v_hist_ace_id = ( SELECT HIST_ACE_ID FROM HIST_ACESSOS WHERE USU_ID = p_usu_id ORDER BY HIST_ACE_ID DESC LIMIT 0, 1 );
  SET v_evento = CONCAT( 'Livro devolvido: "', v_titulo, '"' );
  SET v_descricao = CONCAT( 'Título do livro: "', v_titulo, '"', '|' );
  SET v_descricao = CONCAT( v_descricao, 'Quando: "', p_datahora, '"', '|' );
  CALL P_HIST_PROC_LIV ( v_hist_ace_id, p_usu_id, v_evento, v_descricao, p_liv_id );

end
;;
DELIMITER ;