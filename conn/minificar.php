<?php

require_once( $path_raiz.'/ferr/packer/class.JavaScriptPacker.php' );


// comprime buffer de página com ob_start(comprimir_pagina)

## Convert Single Line Comment to Block Comments
function singleLineComments( &$output ) {
	$output = preg_replace_callback('#//(.*)#m',
	create_function(
		'$match',
		'return "/* " . trim(mb_substr($match[1], 0)) . " */";'
		), $output
	);
}


//  função que trata de minificar o buffer de saída do navegador

function comprimir_pagina($buffer) {

	// as linhas abaixo removem os casos em que "//" não é um comentário. Ao final dessa rotina, esses dados são devolvidos ao buffer
	
	$busca_1 = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
	$tem_busca_1 = false;
	
	$busca_2 = '<html xmlns="http://www.w3.org/1999/xhtml">';
	$tem_busca_2 = false;
	
	$busca_3 = 'http://';
	$busca_3_r = 'http:##';
	$tem_busca_3;
	
	$busca_4 = 'https://';
	$busca_4_r = 'https:##';
	$tem_busca_4;
	
	$busca_5 = 'rtmp://';
	$busca_5_r = 'rtmp:##';
	$tem_busca_5;
	
	if ( !( strpos( $buffer, $busca_1 ) === false ) ) {
		$tem_busca_1 = true;
		$buffer = str_replace( $busca_1, '', $buffer );
	}
	
	if ( !( strpos( $buffer, $busca_2 ) === false ) ) {
		$tem_busca_2 = true;
		$buffer = str_replace( $busca_2, '', $buffer );
	}
	
	if ( !( strpos( $buffer, $busca_3 ) === false ) ) {
		$tem_busca_3 = true;
		$buffer = str_replace( $busca_3, $busca_3_r, $buffer );
	}
	
	if ( !( strpos( $buffer, $busca_4 ) === false ) ) {
		$tem_busca_4 = true;
		$buffer = str_replace( $busca_4, $busca_4_r, $buffer );
	}
	
	if ( !( strpos( $buffer, $busca_5 ) === false ) ) {
		$tem_busca_5 = true;
		$buffer = str_replace( $busca_5, $busca_5_r, $buffer );
	}
	
	

	singleLineComments( $buffer ); // converte todo comentário em linha para comentário em bloco
	
	$buffer = preg_replace( '/\/\*[^\!](.*?)\*\//', '', $buffer ); // remove os comentários em bloco
	
  $buffer = str_replace( array( "\t", '  ', '    ', '     ' ), '', $buffer ); // remove tabs e expaços duplos	
  $buffer = str_replace( array( "\n\n", "\n\n\n", "\n\n\n\n" ), "\n", $buffer ); // remove quebras de linhas duplas
  $buffer = str_replace( array( "\n" ), "", $buffer ); // remove quebras de linhas




	// devolve os valores que foram removidos no início dessa rotina

	if ( $tem_busca_5 ) $buffer = str_replace( $busca_5_r, $busca_5, $buffer );
	if ( $tem_busca_4 ) $buffer = str_replace( $busca_4_r, $busca_4, $buffer );
	if ( $tem_busca_3 ) $buffer = str_replace( $busca_3_r, $busca_3, $buffer );
	if ( $tem_busca_2 ) $buffer = $busca_2.$buffer;
	if ( $tem_busca_1 ) $buffer = $busca_1.$buffer;
	
	$buffer = str_replace( '﻿', '', $buffer );
	
	
	// ofusca o código javascript

	preg_match_all("/<script(.*?)>(.*?)<\/script>/i", $buffer, $output);

	for ( $i = 0; $i < count( $output[ 2 ] ); $i++ ) {
	
		$script = $output[ 2 ][ $i ];
		
//		if ( get_magic_quotes_gpc() ) $script = stripslashes($script);
		$encoding = 62; // 0=none; 10=numeric; 62=normal; 95=high ASCII
		$fast_decode = true;
		$special_char = false;
		$packer = new JavaScriptPacker($script, $encoding, $fast_decode, $special_char);
		$packed = $packer->pack();
		
		$buffer = str_replace( $output[ 2 ][ $i ], $packed, $buffer );
	}
	

	
	// retorna o buffer já trabalhado

  return ( $buffer );
}


// comprime arquivos CSS

function wp_minify_css( $css ) {

	// Normalize whitespace
	$css = preg_replace( '/\s+/', ' ', $css );

	// Remove comment blocks, everything between /* and */, unless
	// preserved with /*! ... */
	$css = preg_replace( '/\/\*[^\!](.*?)\*\//', '', $css );

	// Remove space after , : ; { }
	$css = preg_replace( '/(,|:|;|\{|}) /', '$1', $css );

	// Remove space before , ; { }
	$css = preg_replace( '/ (,|;|\{|})/', '$1', $css );

	// Strips leading 0 on decimal values (converts 0.5px into .5px)
	$css = preg_replace( '/(:| )0\.([0-9]+)(%|em|ex|px|in|cm|mm|pt|pc)/i', '${1}.${2}${3}', $css );

	// Strips units if value is 0 (converts 0px to 0)
	$css = preg_replace( '/(:| )(\.?)0(%|em|ex|px|in|cm|mm|pt|pc)/i', '${1}0', $css );

	// Converts all zeros value into short-hand
	$css = preg_replace( '/0 0 0 0/', '0', $css );

	return apply_filters( 'wp_minify_css', $css );

}

?>