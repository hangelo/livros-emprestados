<?php


$_PARAM_ind = 1;


// converte parâmetro de string para vetor

function entendeParam( &$p ) {
	global $_PARAM_ind;
	global $sepParam;
	$_PARAM_ind = 1;
	return explode( $sepParam, $p );
}


// lê parâmetro na posição INDICE e incrementa índice

function getParamAndNext( &$p ) {
	global $_PARAM_ind;
	return decodeParam( $p[ $_PARAM_ind++ ] );
}
