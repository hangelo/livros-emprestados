<?php

function limparXmlOut( $str ) {
	return strpos( $str, '&' ) === false &&
				 strpos( $str, '<' ) === false &&
				 strpos( $str, '>' ) === false ?
				 $str :
				 '<![CDATA['.$str.']]>';
}
