<?php


// faz include em algumas bibliotecas

require( $path_raiz.'/conn/init.php' );
require( $path_raiz.'/conn/error.php' );
require( $path_raiz.'/conn/cookies.php' );
require( $path_raiz.'/conn/xmldoc.php' );


// sistema de bloqueio de acesso por xss

require( $path_raiz.'/conn/csrf.class.php' );

session_start();
require( $path_raiz.'/conn/csrf_init.php' );
$POST_params = $csrf->form_names( $vet_POST_params, false );
session_write_close();

$xss_confere = true;
if ( !$csrf->check_valid( 'post' ) && !$csrf->check_valid( 'get' ) ) { 
	$xss_confere = false;


	// destroi sessão

	require( $path_raiz.'/conn/logout_xss.php' );


	// cria nova chave para token
	
	session_start();
	$csrf->renew_token_id();
	$token_id = $csrf->get_token_id();
	$token_id__md5 = md5( $token_id );
	$token_value = $csrf->get_token( $token_id );
	$sepParam = $csrf->sepParam();
	$POST_params = $csrf->form_names( $vet_POST_params, false );
	session_write_close();


	// encerra execução

	echo 'token inválido';
	exit;
}

require( $path_raiz.'/conn/csrf.php' );



require( $path_raiz.'/conn/cript.php' );
require( $path_raiz.'/conn/params.php' );

require( $path_raiz.'/conn/conpdo.class.php' );

