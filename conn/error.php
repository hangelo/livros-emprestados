<?php


// salva a mensagem de erro em uma rquivo

function er_salva_arquivo( $err, $errfile, $errline, $errstr ) {
	
	global $_SERVER;
	
	$_CONFIG[ 'errorHandler' ][ 'siteName' ] = 'Gestão Interna - iArremate'; // Nome do site?
	
	$mensagem = '';
	$mensagem .= '[ ERRO NO PHP ]'."\r\n";
	$mensagem .= 'Site: '.$_CONFIG[ 'errorHandler' ][ 'siteName' ]."\r\n";
	$mensagem .= 'Tipo de erro: '.$err."\r\n";
	$mensagem .= 'Arquivo: '.$errfile."\r\n";
	$mensagem .= 'Linha: '.$errline."\r\n";
	$mensagem .= 'Descrição: '.$errstr."\r\n";
	if (isset($_SERVER[ 'REMOTE_ADDR' ])) {
		$mensagem .= "\r\n".'[ DADOS DO VISITANTE ]'."\r\n";
		$mensagem .= 'IP: '.$_SERVER[ 'REMOTE_ADDR' ]."\r\n";
		$mensagem .= 'User Agent: '.$_SERVER[ 'HTTP_USER_AGENT' ]."\r\n";
		$mensagem .= 'Funcionário: '.( isset( $_SESSION[ 'LOGIN_FunNOME' ] ) ? $_SESSION[ 'LOGIN_FunNOME' ] : '' )."\r\n";
		$mensagem .= 'Usuário: '.( isset( $_SESSION[ 'LOGIN_UsuNOME' ] ) ? $_SESSION[ 'LOGIN_UsuNOME' ] : '' )."\r\n";
		$mensagem .= 'Quando se logou: '.( isset( $_SESSION[ 'LOGIN_DataHoraLOGADO' ] ) ? $_SESSION[ 'LOGIN_DataHoraLOGADO' ] : '' )."\r\n";
	}
	if ( isset( $_SERVER[ 'REQUEST_URI' ] ) ) { $mensagem .= "\r\n".'URL: '.( ( ( preg_match( '/HTTPS/i', $_SERVER[ "SERVER_PROTOCOL" ] ) ) ? 'https' : 'http' ).'://'.$_SERVER[ 'HTTP_HOST' ].$_SERVER[ 'REQUEST_URI' ] )."\r\n"; }
	if ( isset( $_SERVER[ 'HTTP_REFERER' ] ) ) { $mensagem .= 'Referer: '.$_SERVER[ 'HTTP_REFERER' ]."\r\n"; }
	$mensagem .= 'Data: '.date( 'd/m/Y H:i:s' )."\r\n";
	$mensagem .= "\r\n\r\n".'--------------------------------------------------------------------------------------------------------'."\r\n\r\n";
	
	
	// Começa assunto do erro
	
	$assunto = '[ '.$err.' ] '.$_CONFIG[ 'errorHandler' ][ 'siteName' ].' - '.date('d/m/y H:i:s');
	
	
	// salva no arquivo
	
	$path_raiz = str_replace( ( ( strpos( $_SERVER[ 'SCRIPT_NAME' ], '/~' ) !== false ) ? substr( $_SERVER[ 'SCRIPT_NAME' ], strpos( $_SERVER[ 'SCRIPT_NAME' ], '/', 1 ) ) : $_SERVER[ 'SCRIPT_NAME' ] ), '', $_SERVER[ 'SCRIPT_FILENAME' ] );
	$arq = $path_raiz.'/error/'.$err.'-'.date( 'Ymd' ).'.log';
	
	file_put_contents( $arq, $mensagem, FILE_APPEND );
}


/* Arquivo para manipulação de erros de forma segura */

function manipuladorErros( $errno, $errstr='', $errfile='', $errline='' ) {
	
	
	if ( error_reporting() == 0) return;

	

	// Verifica se não foi chamada por uma 'exception'
	
	if ( func_num_args() == 5 ) {
		$exception = null;
		list( $errno, $errstr, $errfile, $errline ) = func_get_args();
		//$backtrace = array_reverse(debug_backtrace());
	} else {
		$exc = func_get_arg( 0 );
		$errno = $exc->getCode(); // Nome do erro
		$errstr = $exc->getMessage(); // Descrição do erro
		$errfile = $exc->getFile(); // Arquivo
		$errline = $exc->getLine(); // Linha
		//$backtrace = $exc->getTrace();
	}
	// A variável $backtrace pode ser usada para fazer um Back Trace do erro


	// "Nome" de cada tipo de erro
	
	$errorType = array (
		E_ERROR => 'ERROR',
		E_WARNING => 'WARNING',
		E_PARSE => 'PARSING ERROR',
		E_NOTICE => 'NOTICE',
		E_CORE_ERROR => 'CORE ERROR',
		E_CORE_WARNING => 'CORE WARNING',
		E_COMPILE_ERROR => 'COMPILE ERROR',
		E_COMPILE_WARNING => 'COMPILE WARNING',
		E_USER_ERROR => 'USER ERROR',
		E_USER_WARNING => 'USER WARNING',
		E_USER_NOTICE => 'USER NOTICE',
		E_STRICT => 'STRICT NOTICE',
		E_RECOVERABLE_ERROR => 'RECOVERABLE ERROR'
	);


	// Define o "nome" do erro atual
	
	if ( array_key_exists( $errno, $errorType ) ) {
		$err = $errorType[ $errno ];
	} else {
		$err = 'CAUGHT EXCEPTION';
	}


	if ( $err == 'STRICT NOTICE' ) return true; // ignora este tipo de erro
	else {
			
		// Se está ativo o LOG de erros, salva uma mensagem no log, usando o formato padrão
		
		if ( ini_get( 'log_errors' ) ) error_log( sprintf( "PHP %s:  %s in %s on line %d", $err, $errstr, $errfile, $errline ) );
	
		
		// salva em arquivo
	
		//er_salva_arquivo( $err, $errfile, $errline, $errstr );
	
	
		// Mensagem simples
		
		$mensagemSimples = $err.': '.$errstr.' no arquivo '.$errfile.' (Linha '.$errline.')';
	
	
		// retorno da função
		
		return true;
	}
}


//error_reporting( E_ALL | E_STRICT );
error_reporting( E_ALL );
ini_set( 'display_errors', 1 );
//set_error_handler( 'manipuladorErros' ); // Define o seu novo manipulador de erros
