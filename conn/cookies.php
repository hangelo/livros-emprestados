<?php

$CRYPT_secret_key = 'BDxSQyb}m=32q3+ny\*.m@PZAy7%GUX';


// criptografa informações para serem salvas em um cookie

function encode_cookie( $str ) {
	return $str;
}


// decriptografa informações de um cookie

function decode_cookie( $str ) {
	return $str;
}


// croa uma cookie

function criar_cookie( $nome, $valor ) {
	global $COOKIE_url;
	$T_30dias = time() + 60 * 60 * 24 * 30;
	setcookie( $nome, $valor, $T_30dias, '/', $COOKIE_url, false );
}


// resgata uma cookie

function resgatar_cookie( $nome ) {
	if ( isset( $_COOKIE[ $nome ] ) ) return $_COOKIE[ $nome ];
	else return false;
}
