<?php


// dimensões das fotos

$vet_dimensoes = array(
	array( 2000, 2000 ),
	array( 1000, 1000 ),
	array( 600, 600 ),
	array( 760, 430 ), // foto principal na página de conteúdo do site
	array( 304, 455 ), // destaque primário na index
	array( 194, 194 ), // destaque secundário na index
	array( 186, 260 ), // thumbnail na página de relação
	//140x100
	array( 100, 100 ), // thumbnail para relação no painel de controle
	array( 60, 60 )
);


// constantes

$URL_fotos = 'https://s3-sa-east-1.amazonaws.com/imgs.saolourencoturismo/';


// ajusta string formando nome de arquivo de imagem segundo dimensão

function ajusta_dimensao_foto( $arq, $dimensao ) {
	$v = explode( '/', $arq );
	$a = $v[ count( $v ) - 1 ];
	$r = str_replace( $a, str_replace( '.jpg', '', $a ).'.'.$dimensao.'.jpg', $arq );
	return $r;
}


