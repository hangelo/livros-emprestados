<?php


// função que decodifica parâmetros

function decodeParam( $str ) {
	global $token_id__md5;
	
	$c = $token_id__md5; // esta máscara deve ser igual à usada no arquivo no arquivo XMLDOC.JS
	$n = 7739; // esta máscara deve ser igual à usada no arquivo no arquivo XMLDOC.JS
	$f = '5709tP29DlplSHFECc4By'; // esta máscara deve ser igual à usada no arquivo no arquivo XMLDOC.JS

	if ( strpos( $str, $c ) !== false ) {
		$str = str_replace( $c, '', $str );
		$str = str_replace( $f, '', $str );
		$str = (int)$str - (int)$n;
	}
	return $str;
}


// função que codifica parâmetros

function encodeParam( $str ) {
	global $token_id__md5;
	
	$c = $token_id__md5; // esta máscara deve ser igual à usada no arquivo no arquivo XMLDOC.JS
	$n = 7739; // esta máscara deve ser igual à usada no arquivo no arquivo XMLDOC.JS
	$f = '5709tP29DlplSHFECc4By'; // esta máscara deve ser igual à usada no arquivo no arquivo XMLDOC.JS

	if ( preg_match( '/^\d+$/', $str ) ) {
		$str = $c.( (int)$str + (int)$n ).$f;
	}

	return $str;
}
