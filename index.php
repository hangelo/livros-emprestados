<?php


// entende o PATH RAIZ do site

$path_raiz = str_replace( ( ( strpos( $_SERVER[ 'SCRIPT_NAME' ], '/~' ) !== false ) ? substr( $_SERVER[ 'SCRIPT_NAME' ], strpos( $_SERVER[ 'SCRIPT_NAME' ], '/', 1 ) ) : $_SERVER[ 'SCRIPT_NAME' ] ), '', $_SERVER[ 'SCRIPT_FILENAME' ] );


// carrega bibliotecas, demais funções e variáveis

require( $path_raiz.'/conn/requires_pg.php' );


// se parâmetro de bloqueio XSS não confere, exibe tela de login

if ( $xss_confere ) {
	require( $path_raiz.'/conn/verifica_login.php' );
}


// se não estiver logado, exibe tela de login

if ( !$xss_confere || !$_LOGIN__logado ) {
	require( $path_raiz.'/pg/login.php' );
}


// se está logado, continua carregando

else {
	require( $path_raiz.'/pg/index.php' );
}




