<?php


// entende o PATH RAIZ do site

$path_raiz = str_replace( ( ( strpos( $_SERVER[ 'SCRIPT_NAME' ], '/~' ) !== false ) ? substr( $_SERVER[ 'SCRIPT_NAME' ], strpos( $_SERVER[ 'SCRIPT_NAME' ], '/', 1 ) ) : $_SERVER[ 'SCRIPT_NAME' ] ), '', $_SERVER[ 'SCRIPT_FILENAME' ] );


// carrega bibliotecas, demais funções e variáveis

require( $path_raiz.'/conn/conpdo.class.php' );


// variáveis

$nome = 'Angelo';
$usuario = 'admin';
$senha = '123456';
$email = 'angelo.hzp@gmail.com';


// cria usuário

$padrao = 'whirlpool_double';
$salt = generate_salt( generator_rand( 8, 32 ) );
$senha = criptografar_whirlpool__doubleSalt( $salt, $senha );

$tipo = 1;
$timezone = -3;
	
try {
	inicia_transacao( $conexao, $transaction );						

	$qry = $conexao->prepare( "CALL P_USU_ADD ( :nome, :usuario, :senha, :salt, :padrao );" );
	$qry->bindParam( ':nome', $nome );
	$qry->bindParam( ':usuario', $usuario );
	$qry->bindParam( ':senha', $senha );
	$qry->bindParam( ':salt', $salt );
	$qry->bindParam( ':padrao', $padrao );
	$qry->execute();

	commit_transacao( $conexao, $transaction );
} catch ( Exception $e ) { rollback_transacao( $conexao, $transaction, $e->getMessage() ); }


header( "Content-Type: text/plain" );
ob_clean();
ob_start();
echo 'criado com sucesso';
ob_end_flush();