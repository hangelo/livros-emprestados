# SOBRE #

Este é um sistema básico de controle de livros, criado como um teste de vaga de trabalho.
Neste eu utilizo algumas classes feitas por mim, como conexão para banco de dados e proteções para XXS.

# DÚVIDAS E CONTRIBUIÇÕES #

Henrique Angelo
angelo.hzp@gmail.com

# INSTRUÇÕES

### Configurar banco de dados, arquivos ###

* conn/conpdo.class.php

### Configurar criptografias, arquivos ###

* conn/cript.php
* conn/csrf.class.php
* conn/csrf_init.php
* conn/conpdo.class.php

### Conexão com banco de dados ###

O banco de dados é configurado em:

```
#!PHP
private static $hostname = 	'HOST';
private static $database = 	'DATABASE';
private static $username = 	'USERNAME';
private static $password = 	'PASSWORD';
```

A classe funciona mantendo uma variável com a conexão aberta enquanto puder ser usada, evitando que hajam várias conexão, tirando esse controle do programador e mantendo-o pela classe, técnica chama "singleton".

O controle de transação também é simplificado, permitindo que em apenas uma linha a conexão seja feita e a transação iniciada. O mesmo vale para dar um commit ou rollback.
```
#!PHP
try {
	inicia_transacao( $conexao, $transaction );

	$sql = 'CALL P_LIV_EDIT ( :usu_id, :liv_id, :titulo );';
	$qry = $conexao->prepare( $sql );
	$qry->bindParam( ':usu_id', $_LOGIN__UsuId );
	$qry->bindParam( ':liv_id', $liv_id );
	$qry->bindParam( ':titulo', $titulo );
	$qry->execute();

	commit_transacao( $conexao, $transaction );
} catch ( Exception $e ) { rollback_transacao( $conexao, $transaction, $e->getMessage() ); }
```

É interessante notar que há uma reescrita completa da função "bindParam", permitindo a classe manipular as variáveis inseridas para que sejam usadas depois em um controle de auditoria:
```
#!PHP
salva_consulta_db( 'P_HIST_PROC_SQL_ADD', $sql, $conexao, $qry ); // armazena a consulta em histórico
```
O primeiro parâmetro é a STORED PROCEDURE usada para inserir o evento. O segundo párâmetro é o SQL usado. O terceiro e o quarto são respectivamente a conexão e a query usadas.

### Criptografia ###

O sistema usa para conexão ao banco de dados uma criptografia com "double salt", com strings aleatórias e de tamanhos aleatórios. As chaves principais podem ser reconfiguradas nos arquivos:

**conn/csrf.class.php**
```
#!PHP
function criptografar_whirlpool__doubleSalt( $toHash, $str ){  // criptografa uma senha $str usando WHIRLPOOL com duplo SALT, sendo um fixo e um passado por parâmetro (número aleatório salvo em banco de dados)
	$v_toHash = str_split( $toHash, ( strlen( $toHash ) / 2 ) + 1 ); 
	$hash = hash( 'whirlpool', $str.$v_toHash[ 0 ].'iarremate!prime@#$.angelo'.$v_toHash[ 1 ] ); 
	return $hash; 
}
```

**conn/cript.php**
```
#!PHP
$c = $token_id__md5; // esta máscara deve ser igual à usada no arquivo no arquivo XMLDOC.JS
$n = 7739; // esta máscara deve ser igual à usada no arquivo no arquivo XMLDOC.JS
$f = '5709tP29DlplSHFECc4By'; // esta máscara deve ser igual à usada no arquivo no arquivo XMLDOC.JS
```

Os parâmetros de comunicação usando POST e GET podem ser configurados aqui:
**conn/csrf_init.php**
```
#!PHP
$vet_POST_params = array(
	'pg',
	'a0',
	'b0',
	'c0',
	'e0',
	'd0',
	'f0',
	'g0',
	'h0',
	'i0',
	'j0',
	'k0',
	'l0',
	'm0',
	'n0',
	'o0',
	'p0',
	'q0',
	'r0',
	's0',
	't0',
	'u0',
	'v0',
	'w0',
	'x0',
	'y0',
	'z0',
);
```
Eu particularmente gosto de usar esses, embora na maioria das vezes acabo usando apenas três ou quatro. O arquivo **conn/csrf.php** se encarregar de pegar esse vetor e recria-lo para ser entendido também quando usado em javascript.

## Sistema de autenticação ##

A autenticação é feita comparando um token salvo na sessão do PHP com informações salvas em cookie e através dos parâmetros GET de uma URL.
Toda variável precisa de um identificador.

**Exemplo:**
Ao passar usuário e senha para login, são necessários os identificadores "usu" e "sen" (apenas exemplos) com os valores de usuário e senha digitados. A classe **CSRF** se encarregar de criar um identificador aleatório para o que antes eram "usu" e "sen", dificultando que alguém possa fazer uma injeção cross-site. Além disso, quando os valores são enviados ao servidor, é passado junto um token, com seu identificador criptografado da mesma maneira. Se o token não corresponder em ambos os lados, cliente e servidor, é entendido violação no acesso e toda a estrutura da sessão cai, gerando identificadores completamente novos e consequentemente caindo a sessão do usuário.

Esta técnica pode ser aprimorada com o uso de sockets. Criando um servidor que mantenha uma conexão aberta com o cliente, pode atualizar os identificadores à cada requisição, deixando impossível alguém conseguir rastrear a chave, pois ela se altera à cada interação com o servidor.

## Primeira utilização ##

Execute o arquivo **iniciar.php** para criar o primeiro usuário. No arquivo, edite o usuário e a senha

```
#!PHP
$nome = 'Angelo';
$usuario = 'admin';
$senha = '123456';
$email = 'angelo.hzp@gmail.com';
```
Após executar esse arquivo, delete-o do servidor.

## Escrever o banco de dados ##

O banco de dados está no arquivo **database.sql**. Após escreve-lo, delete também este arquivo.