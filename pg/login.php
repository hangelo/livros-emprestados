<?php


// minifica o conteúdo antes de exibir em tela

//require( $path_raiz.'/conn/minificar.php' );
ob_start();



echo '<!DOCTYPE><html><head>

	<meta charset="utf-8">
	<title>Login</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link href="http//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" id="main-css"/>
	<link href="http://fonts.googleapis.com/css?family=Josefin+Sans:100,300,400,600,700" rel="stylesheet">
	<link href="/css/login.css" rel="stylesheet">

	<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
	<script src="/js/xmldoc.js"></script>
	<script src="/js/login.js"></script>
	<script src="/js/cookie.js"></script>

	'.$js_POST_params.'

</head><body ng-app="postLogin" ng-controller="PostController as postCtrl">

	<div class="boxLogin">
		<div class="in">
			<form name="login" id="login" ng-submit="postCtrl.postForm()" class="form-horizontal" method="POST">

				<div class="tit">Entre em sua conta</div>
				<div class="lin">
					<input type="text" class="inputText" id="in_usuario" placeholder="" required autofocus ng-model="postCtrl.inputData.us" />
					<label for="in_usuario">Email de acesso</label>
					<div class="err" id="err"></div>
				</div>
				<div class="lin">
					<input type="password" class="inputText" id="in_senha" placeholder="" required ng-model="postCtrl.inputData.ps" />
					<label for="in_senha">Senha</label>
				</div>

				<div ng-show="erMsg" class="err">Erro: {{errorMsg}}</div>

				<div class="barBtn">
					<button type="submit" class="btnSubmit" ng-disabled="login.$invalid">Entrar</button>
				</div>

			</form>
		</div>
	</div>


</body></html>';
