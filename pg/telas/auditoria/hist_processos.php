<?php


// consultas ao banco de dados

try {
	inicia_transacao( $conexao, $transaction );


	// pega informações dos noticias

	$HTML_hist = '';

	$sql = 'SELECT * FROM HIST_PROCESSOS WHERE HIST_PROC_ID >= 450 ORDER BY HIST_PROC_DATAHORA_EVENTO DESC;';
	$qry = $conexao->prepare( $sql );
	$qry->execute();
	while( $r = $qry->fetch( PDO::FETCH_ASSOC ) ) {

		$datahora_evento = preg_replace( '/(\d{4})\-(\d{2})\-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/', '$3/$2/$1 $4:$5', $r[ 'HIST_PROC_DATAHORA_EVENTO' ] );
		$evento = $r[ 'HIST_PROC_EVENTO' ];
		$descricao = $r[ 'HIST_PROC_DESCRICAO' ];

		$HTML_hist .= '
			<tr>
				<td><div class="tdIte">'.$datahora_evento.'</div></td>
				<td><div class="tdIte"><b>'.$evento.'</b><br>'.$descricao.'</div></td>
			</tr>
		';

	}


	commit_transacao( $conexao, $transaction );
} catch ( Exception $e ) { rollback_transacao( $conexao, $transaction, $e->getMessage() ); }


// ajustes finais para formar grade de valores

$HTML_hist = '<tbody class="scrollContent">'.$HTML_hist.'</tbody>';

$HTML_thead = '
	<thead class="fixedHeader">
		<tr>
			<th><div class="tdIte"><a>Data/Hora</a></div></th>
			<th><div class="tdIte"><a>Evento</a></div></th>
		</tr>
	</thead>
';

$HTML_grade = '';
$HTML_grade .= '
	<div id="tableContainer" class="tableContainer">
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			'.$HTML_thead.'
			'.$HTML_hist.'
		</table>
</div>
';


// atualiza título da janela

$HEADER_add_titulo = 'Histórico de Processos';


// informa para adicionar vínculo à JS e CSS

$HEADER_add_css .= ',hist_acessos';
$HEADER_add_js = '';


$HTML_barMenu = '
	<a href="/?'.$POST_init_param.'&'.$POST_params[ 'pg' ].'=ha"><div class="menu">Atualizar</div></a>
';


// saída

$HTML_SAIDA .= $HTML_grade;


