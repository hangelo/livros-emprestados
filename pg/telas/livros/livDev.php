<?php


// entende o PATH RAIZ do site

$path_raiz = str_replace( ( ( strpos( $_SERVER[ 'SCRIPT_NAME' ], '/~' ) !== false ) ? substr( $_SERVER[ 'SCRIPT_NAME' ], strpos( $_SERVER[ 'SCRIPT_NAME' ], '/', 1 ) ) : $_SERVER[ 'SCRIPT_NAME' ] ), '', $_SERVER[ 'SCRIPT_FILENAME' ] );


// carrega bibliotecas, demais funções e variáveis

require( $path_raiz.'/conn/requires_cmd.php' );


// carrega controle de login

require( $path_raiz.'/conn/verifica_login.php' );


if ( !$xss_confere || !$_LOGIN__logado ) {
	echo 'logar novamente';
	exit;
}


// recebe parâmetros

$param = isset( $_POST[ $POST_params[ 'a0' ] ] ) ? $_POST[ $POST_params[ 'a0' ] ] : NULL;
if ( !$param ) { echo 'falha no parâmetro';	exit; }


// entende parâmetros

$p = entendeParam( $param );
$liv_id = getParamAndNext( $p );


// consulta usuário e faz login

try {
	inicia_transacao( $conexao, $transaction );
		
	$qry = $conexao->prepare( "CALL P_LIV_DEV ( :usu_id, :liv_id, CURRENT_TIMESTAMP );" );
	$qry->bindParam( ':usu_id', $_LOGIN__UsuId );
	$qry->bindParam( ':liv_id', $liv_id );
	$qry->execute();

	commit_transacao( $conexao, $transaction );
} catch ( Exception $e ) { rollback_transacao( $conexao, $transaction, $e->getMessage() ); }


// retorno

header( "Content-Type: text/plain" );
ob_clean();
ob_start();
echo 'ok'.$sepParam;
ob_end_flush();
















