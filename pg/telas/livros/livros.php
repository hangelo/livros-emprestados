<?php


// consultas ao banco de dados

try {
	inicia_transacao( $conexao, $transaction );
	

	// pega somente livros emprestados

	$HTML_livros = '';

	$status = 1; // 2=emprestado

	$sql = 'SELECT LIVROS.LIV_ID, LIV_NUMERO, LIV_TITULO, LIV_STATUS, LIV_ULT_EMPRESTIMO_SAIU, LIV_ULT_EMPRESTIMO_VOLTOU FROM LIVROS ORDER BY LIV_TITULO;';
	$qry = $conexao->prepare( $sql );
	$qry->execute();
	while( $r = $qry->fetch( PDO::FETCH_ASSOC ) ) {

		$liv_id = $r[ 'LIV_ID' ];
		$numero = $r[ 'LIV_NUMERO' ];
		$titulo = $r[ 'LIV_TITULO' ];

		$emprestimos = '';
		$qry2 = $conexao->prepare( 'SELECT LIV_EMP_STATUS, LIV_EMP_QUEM, LIV_EMP_DATAHORA_SAIU, LIV_EMP_DATAHORA_VOLTOU FROM LIVROS_EMPRESTIMOS WHERE LIV_ID = '.$liv_id.' ORDER BY LIV_EMP_DATAHORA_SAIU DESC LIMIT 0, 4;' );
		$qry2->execute();
		while( $r2 = $qry2->fetch( PDO::FETCH_ASSOC ) ) {
			$quem = $r2[ 'LIV_EMP_QUEM' ];
			$saiu = preg_replace( '/(\d{4})\-(\d{2})\-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/', '$3/$2/$1 $4:$5', $r2[ 'LIV_EMP_DATAHORA_SAIU' ] );
			$voltou = preg_replace( '/(\d{4})\-(\d{2})\-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/', '$3/$2/$1 $4:$5', $r2[ 'LIV_EMP_DATAHORA_VOLTOU' ] );
			$_status = $r2[ 'LIV_EMP_STATUS' ];

			$emprestimos .= '<div class="sIte">'.$saiu.' - '.( $_status == '2' ? $voltou : '<span ng-click="livCtrl.doDevolver( '.$liv_id.', \''.$titulo.'\' );">Devolver</span>' ).'<br>'.$quem.'</div>';
		}

		if ( $emprestimos ) $emprestimos = '<br>'.$emprestimos;

		$ult_emp_saiu = $r[ 'LIV_ULT_EMPRESTIMO_SAIU' ];
		$ult_emp_voltou = $r[ 'LIV_ULT_EMPRESTIMO_VOLTOU' ];
		$status = $r[ 'LIV_STATUS' ];

		switch ( $status ) {
			case '1' :
				$status = 'Último empréstimo: '.$ult_emp_saiu.' - '.$ult_emp_voltou;
				$_btn = '<a><div class="tdBtn emp" ng-click="livCtrl.doEmprestar( '.$liv_id.', \''.str_replace( array( '"', '\'' ), array( '%22', '%27' ), $titulo ).'\' );">Emprestar</div></a>';
				break;

			case '2' :
				$status = 'Aguardando devolução: '.$ult_emp_saiu;
				$_btn = '<a><div class="tdBtn dev" ng-click="livCtrl.doDevolver( '.$liv_id.', \''.str_replace( array( '"', '\'' ), array( '%22', '%27' ), $titulo ).'\' );">Devolver</div></a>';
				break;
		}

		$HTML_livros .= '
			<tr>
				<td><div class="tdChk"><input type="checkbox" /></div></td>
				<td><div class="tdIte" id="livNum'.$liv_id.'">'.$numero.'</div></td>
				<td><div class="tdIte" id="livTit'.$liv_id.'">'.$titulo.$emprestimos.'</div></td>
				<td>
					<a><div class="tdBtn edt" ng-click="livCtrl.doEdit( '.$liv_id.', \''.str_replace( array( '"', '\'' ), array( '%22', '%27' ), $titulo ).'\' );">Editar</div></a>
					'.$_btn.'
				</td>
			</tr>
		';

	}


	commit_transacao( $conexao, $transaction );
} catch ( Exception $e ) { rollback_transacao( $conexao, $transaction, $e->getMessage() ); }


// ficha de um livro

$HTML_livro = '
<div class="livFicha_bg" id="livFicha_bg" style="display:none;"></div>
<div class="livFicha" id="livFicha" style="display:none;">
	<form name="fchLivro" id="fchLivro" ng-submit="livCtrl.postForm_adicionar()" class="form-horizontal" method="POST">

		<input type="hidden" class="inputText" ng-model="livAdd_livId" />

		<div class="tit">Ficha do livro</div>
		<div class="lin">
			<input type="text" class="inputText" id="in_titulo" ng-model="livCtrl.tit" />
			<label for="in_titulo">Título do livro</label>
		</div>

		<div ng-show="erMsg" class="err">Erro: {{errorMsg}}</div>

		<div class="barBtn">
			<button type="button" class="btnCancelar" ng-click="livCtrl.cancelForm_adicionar();">Cancelar</button>
			<button type="submit" class="btnSalvar" ng-disabled="fchLivro.$invalid">Salvar</button>
			<div style="clear:both;"></div>
		</div>

	</form>
</div>
';


// emprestar um livro

$HTML_emprestar = '
<div class="livEmprestar_bg" id="livEmprestar_bg" style="display:none;"></div>
<div class="livEmprestar" id="livEmprestar" style="display:none;">
	<form name="fchEmprestar" id="fchEmprestar" ng-submit="livCtrl.postForm_emprestar()" class="form-horizontal" method="POST">

		<div class="tit">Emprestar um livro</div>
		<div class="inf">{{livTitulo_emprestar}}</div>

		<input type="hidden" class="inputText" ng-model="livEmp_livId" />

		<div class="lin">
			<input type="text" class="inputText" id="in_quem" ng-model="livCtrl.quem" />
			<label for="in_quem">Nome da pessoa</label>
		</div>

		<div class="barBtn">
			<button type="button" class="btnCancelar" ng-click="livCtrl.cancelForm_emprestar();">Cancelar</button>
			<button type="submit" class="btnSalvar" ng-disabled="fchEmprestar.$invalid">Salvar</button>
			<div style="clear:both;"></div>
		</div>

	</form>
</div>
';


// devolver um livro

$HTML_devolver = '
<div class="livDevolver_bg" id="livDevolver_bg" style="display:none;"></div>
<div class="livDevolver" id="livDevolver" style="display:none;">
	<form name="fchDevolver" id="fchDevolver" ng-submit="livCtrl.postForm_devolver()" class="form-horizontal" method="POST">

		<div class="tit">Confirma devolução do livro?</div>
		<div class="inf">{{livTitulo_devolver}}</div>

		<input type="hidden" class="inputText" ng-model="livEmp_DevId" />

		<div class="barBtn">
			<button type="button" class="btnCancelar" ng-click="livCtrl.cancelForm_devolver();">Não</button>
			<button type="submit" class="btnSalvar" ng-disabled="fchDevolver.$invalid">Sim</button>
			<div style="clear:both;"></div>
		</div>

	</form>
</div>
';


// grade com relação de livros

$HTML_livros = '<tbody class="scrollContent">'.$HTML_livros.'</tbody>';

$HTML_thead = '
	<thead class="fixedHeader">
		<tr>
			<th><div class="tdIte"><a></a></div></th>
			<th><div class="tdIte"><a>Número</a></div></th>
			<th><div class="tdIte"><a>Título</a></div></th>
			<th><div class="tdIte"><a></a></div></th>
		</tr>
	</thead>
';

$HTML_grade = '';
$HTML_grade .= '
	<div id="tableContainer" class="tableContainer">
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			'.$HTML_thead.'
			'.$HTML_livros.'
		</table>
</div>
';


// atualiza título da janela

$HEADER_add_titulo = 'Histórico de acessos';


// informa para adicionar vínculo à JS e CSS

$HEADER_add_css .= ',livros';
$HEADER_add_js = '';

$HTML_barMenu = '
	<a onclick="add();"><div class="menu">Inserir</div></a>
	<a onclick="edit();"><div class="menu">Editar</div></a>
	<a onclick="del();"><div class="menu">Remover</div></a>
';


// saída

$HTML_SAIDA .= '<div ng-app="livFicha" ng-controller="livController as livCtrl">'.$HTML_livro.$HTML_emprestar.$HTML_devolver.$HTML_grade.'</div>';


