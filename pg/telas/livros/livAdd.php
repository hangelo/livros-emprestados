<?php


// entende o PATH RAIZ do site

$path_raiz = str_replace( ( ( strpos( $_SERVER[ 'SCRIPT_NAME' ], '/~' ) !== false ) ? substr( $_SERVER[ 'SCRIPT_NAME' ], strpos( $_SERVER[ 'SCRIPT_NAME' ], '/', 1 ) ) : $_SERVER[ 'SCRIPT_NAME' ] ), '', $_SERVER[ 'SCRIPT_FILENAME' ] );


// carrega bibliotecas, demais funções e variáveis

require( $path_raiz.'/conn/requires_cmd.php' );


// carrega controle de login

require( $path_raiz.'/conn/verifica_login.php' );


if ( !$xss_confere || !$_LOGIN__logado ) {
	echo 'logar novamente';
	exit;
}


// recebe parâmetros

$param = isset( $_POST[ $POST_params[ 'a0' ] ] ) ? $_POST[ $POST_params[ 'a0' ] ] : NULL;
if ( !$param ) { echo 'falha no parâmetro';	exit; }


// entende parâmetros

$p = entendeParam( $param );
$titulo = getParamAndNext( $p );


// consulta usuário e faz login

try {
	inicia_transacao( $conexao, $transaction );
		
	$qry = $conexao->prepare( "CALL P_LIV_ADD ( :usu_id, :titulo, @liv_id );" );
	$qry->bindParam( ':usu_id', $_LOGIN__UsuId );
	$qry->bindParam( ':titulo', $titulo );
	$qry->execute();

	$qry = $conexao->prepare( "SELECT @liv_id AS LIV_ID;" );
	$qry->execute();
	$r = $qry->fetch( PDO::FETCH_ASSOC );
	$liv_id = $r[ 'LIV_ID' ];	

	commit_transacao( $conexao, $transaction );
} catch ( Exception $e ) { rollback_transacao( $conexao, $transaction, $e->getMessage() ); }


// retorno

header( "Content-Type: text/plain" );
ob_clean();
ob_start();
echo 'ok'.$sepParam;
ob_end_flush();
















