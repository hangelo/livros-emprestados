<?php


// se foi pedido CSS adicional, entende aqui

$_css = '';
if ( $HEADER_add_css ) {
	$_x = explode( ',', $HEADER_add_css );
	foreach ( $_x as $x ) {
		$_css .= '<link href="/css/'.$x.'.css" rel="stylesheet">';
	}
}


// se foi pedido CSS adicional, entende aqui

$_js = '';
if ( $HEADER_add_js ) {
	$_x = explode( ',', $HEADER_add_js );
	foreach ( $_x as $x ) {
		$_js .= '<script src="/js/'.$x.'.js"></script>';
	}
}


// abre o deader

$head_abre = '';
$head_abre .= '
<!DOCTYPE html><html><head>

	<meta charset="utf-8">

	<title>'.$HEADER_add_titulo.'</title>

	<meta name="robots" content="noindex, nofollow">

	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=1">

	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:100,300,400,600,700" rel="stylesheet">
	
	<link href="/css/index.css" rel="stylesheet">
	<link href="/css/esq.css" rel="stylesheet">
	'.$_css.'

	<script src="/js/xmldoc.js"></script>
	<script src="/js/cookie.js"></script>

	'.$HEADER_variaveis_header.'

	<script src="/js/index.js"></script>
	<script src="/js/esq.js"></script>

	'.$_js.'

';


// fecha o HEAD e abre o BODY

$body_abre = '';
$body_abre .= '</head><body>';


// fecha o BODY e HTML

$body_fecha = '';
$body_fecha .= $HEADER_variaveis_body_final;
$body_fecha .= '</body></html>';