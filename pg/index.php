<?php


// biblioteca para fotos

require( $path_raiz.'/conn/fotos.php' );


// minifica o conteúdo antes de exibir em tela

ob_start();


// inicializa variáveis de saída

$HEADER_add_titulo = 'Painel de Gestão'; // título da página
$HEADER_add_css = ''; // CSS separados por vírgula
$HEADER_add_js = ''; // JS separados por vírgula
$HEADER_variaveis_header = ''; // conteúdo JavaScript no HEAD
$HEADER_variaveis_body_final = ''; // conteúdo JavaScript no final do BODY
$HTML_SAIDA = ''; // conteúdo HTML da página
$HTML_barMenu = ''; // contém menu da página


// informa para adicionar vínculo à JS e CSS

$HEADER_add_css = 'index,grade';
$HEADER_add_js = 'xmldoc';


// reconhece a página que será carregada

$pag = isset( $_GET[ $POST_params[ 'pg' ] ] ) ? decodeParam( $_GET[ $POST_params[ 'pg' ] ] ) : NULL;

switch ( $pag ) {
	case 'ha' :	require( $path_raiz.'/pg/telas/auditoria/hist_acessos.php' ); break;
	case 'hp' :	require( $path_raiz.'/pg/telas/auditoria/hist_processos.php' ); break;
	default :	require( $path_raiz.'/pg/telas/livros/livros.php' ); break;
}


// bibliotecas javascript

$HEADER_variaveis_header = '<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>';


// adiciona includes de PHP

require( $path_raiz.'/pg/inc/header.php' );
require( $path_raiz.'/pg/inc/esq.php' );





// escreve página

echo $head_abre; // abre o HTML e HEAD

echo $js_POST_params; // escreve controle JavaScript sobre parâmetros passados por POST ou GET

echo $body_abre; // simplesmente fecha o HEADE e abre o BODY

echo $esq; // menu principal do site

echo '
<div class="top">
	<div class="barMenu" id="barMenu" style="margin-left:360px;">
		'.$HTML_barMenu.'
	</div>
</div>
';

echo '<div class="pg" id="pg" style="margin-left:280px;">'.$HTML_SAIDA.'</div>'; // conteúdo HTML da página

echo $body_fecha; // fecha o BODY e HTML
